﻿using DesertImage.Entities;
using UnityEngine;

namespace Tutorial.Events
{
    public struct EndStepTutorialEvent
    {
        public IEntity Value;
        public Transform PosTo;
        public bool IsNextDontMoveStep;
        public bool IsEndStep;
    }
}