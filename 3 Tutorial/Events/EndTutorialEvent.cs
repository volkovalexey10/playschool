﻿using DesertImage.Entities;

namespace Tutorial.Events
{
    public struct EndTutorialEvent
    {
        public bool IsComplete;
        public bool IsStepComplete;
        public IEntity DragItem;
        public IEntity TargetItem;
    }
}