﻿using DesertImage.Entities;
using UnityEngine;

namespace Tutorial.Events
{
    public struct StartTutorialEvent
    {
        public IEntity Item;
        public bool IsDontCycle;
        public bool IsNextStep;
    }
}