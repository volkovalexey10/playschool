﻿using DesertImage.Entities;
using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;

namespace Tutorial.Widgets
{
    public class HandAboveWidget : MonoBehaviourPoolable
    {
        [SerializeField] private SkeletonAnimation handSkeleton;
        [SerializeField] private SpriteRenderer spriteItem;

        public Transform Transform => transform;

        public SkeletonAnimation HandSkeleton => handSkeleton;

        public SpriteRenderer SpriteItem => spriteItem;
    }
}