﻿using DesertImage;
using Tutorial.Events;
using Tutorial.Widgets;

namespace Tutorial.Base
{
    public interface ITutorial : IListen<EndTutorialEvent>
    {
        public LTSeq LtSeq { get; set; }
        public string Tap { get; set; }
        public string Idle { get; set; }
        
        public bool IsStartAnim { get; set; }
        public bool IsDragObj { get; set; }
        public bool IsComplete { get; }
        
        void StartTutor();

        void StopTutor();
    }
}