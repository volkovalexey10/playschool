﻿using System;
using Components;
using Kiddopia.DragAndDrop;
using UnityEngine;
using UnityEngine.UI;

namespace Tutorial.Data
{
    [Serializable]
    public class DataTutorial : DataComponent<DataTutorial>
    {
        public ItemsManager ItemsManager;
        public Button MenuButton;
        public Transform[] PointsTo;
    }
}