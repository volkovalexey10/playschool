
using UnityEngine;

namespace Kiddopia.SpeedReading.Events
{
    public struct SpawnNumberEvent
    {
        public string TextNumber;
        public Color ColorBgNumber;
    }
}
