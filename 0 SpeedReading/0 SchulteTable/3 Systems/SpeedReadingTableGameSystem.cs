using System.Collections.Generic;
using System.Linq;
using Components;
using DesertImage;
using DesertImage.Audio;
using DesertImage.Entities;
using DesertImage.Extensions;
using DesertImage.Managers;
using Dots.Events;
using Game.Vibration;
using Interaction;
using Kiddopia.Audio;
using Kiddopia.GameData;
using Kiddopia.Loading;
using Kiddopia.Painting;
using Kiddopia.Spawning;
using Kiddopia.SpeedReading.Data;
using Kiddopia.SpeedReading.DataComponents;
using Kiddopia.SpeedReading.Events;
using Kiddopia.UI;
using MathWhiz.Main.Events;
using Other;
using UnityEngine;
using FactorySoundExtensions = Kiddopia.Spawning.FactorySoundExtensions;

namespace Kiddopia.SpeedReading.Systems
{
    public class SpeedReadingTableGameSystem : SystemBase, IListen<PointerDownEvent>, IListen<RestartPaintEvent>
    {
        private DataSpeedReadingTableStarter _dataStarter;
        private IEntity _prevItem;
        private byte _currentNumber = 1;
        private byte _currentMaxNumber;
        private float _oldRecordTime;
        public override void Activate()
        {
            base.Activate();
            _dataStarter = Core.Instance.Get<DataSpeedReadingTableStarter>();
            _dataStarter.SizeTable = LocalStorage.GameData.DataSpeedReading.SizeTable;
            LocalStorage.GameData.DataSpeedReading.SchulteData.TryGetValue(_dataStarter.SizeTable, out _oldRecordTime);
            _currentMaxNumber = (byte) (_dataStarter.SizeTable * _dataStarter.SizeTable);
            this.ListenGlobalEvent<PointerDownEvent>();
            this.ListenGlobalEvent<RestartPaintEvent>();
        }

        public override void Deactivate()
        {
            base.Deactivate();
            this.UnlistenGlobalEvent<PointerDownEvent>();
            this.UnlistenGlobalEvent<RestartPaintEvent>();
        }


        public void handleCallback(RestartPaintEvent arguments)
        {
            this.SendGlobalEvent(new VibrationSingleEvent());
            _currentNumber = 1;
            this.SendGlobalEvent(new StopTimerEvent());
            this.SendGlobalEvent(new RestartGameEvent());
        }
        public void handleCallback(PointerDownEvent arguments)
        {
            var currentNumber = int.Parse(arguments.Value.Get<DataTMP>().Value.text);
            this.SendGlobalEvent(new VibrationSingleEvent());
            
            if (Check(currentNumber))
            {
                _prevItem?.SendEvent(new MathWhizBorderButtonTransparentEvent());
                
                this.GetRandomValue<SoundId>(
                    (ushort) SoundId.Success1,
                    (ushort) SoundId.Success7).Play2D(SoundPriority.Important);
                
                arguments.Value.SendEvent(new MathWhizBorderButtonCustomEvent
                    {CustomColor = new Color32(0, 146, 42, 255)});
                arguments.Value.Get<DataImage>().Value.raycastTarget = false;
                
                this.SendGlobalEvent(new StartTimerEvent());
                
                if (_currentNumber > _currentMaxNumber)
                {
                    this.SendGlobalEvent(new WinEvent());
                    
                    LocalStorage.GameData.DataSpeedReading.SchulteData.TryGetValue(_dataStarter.SizeTable, out var currentRecord);
                    LocalStorage.GameData.DataSpeedReading.PrevPanel = SpeedReadingPanel.Schulte;
                    
                    if (currentRecord < _oldRecordTime || _oldRecordTime == 0f)
                    {
                        this.SendGlobalEvent(new LevelCompleteEvent()
                        {
                            Reward = 5,
                            OnlyReward = true,
                            UseCustomChangeScene = true,
                            CustomChangeSceneId = ScenesId.SpeedReadingSelector
                        });
                    }
                    else
                    {
                        this.SendGlobalEvent(new LevelCompleteEvent()
                        {
                            ShowButtons = false,
                            UseCustomChangeScene = true,
                            CustomChangeSceneId = ScenesId.SpeedReadingSelector
                        });
                    }
                }
            }
            else
            {
                _prevItem?.SendEvent(new MathWhizBorderButtonTransparentEvent());
                _prevItem = arguments.Value;
                
                var sound = this.GetRandomValue<SoundId>(
                    (ushort) SoundId.Error1,
                    (ushort) SoundId.Error5).Play2D(SoundPriority.Important);
                var time = sound.Clip.length;
                
                arguments.Value.SendEvent(new MathWhizBorderButtonErrorEvent());
                this.DoActionWithDelay(() =>
                {
                    arguments.Value.SendEvent(new MathWhizBorderButtonTransparentEvent());
                }, time);
            }
        }

        private bool Check(int number)
        {
            if (number != _currentNumber) return false;
            _currentNumber++;
            return true;
        }
    }
}
