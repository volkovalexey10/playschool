using System.Linq;
using Components;
using DesertImage;
using DesertImage.Extensions;
using DesertImage.Managers;
using Entities;
using Kiddopia.GameData;
using Kiddopia.Spawning;
using Kiddopia.SpeedReading.DataComponents;
using Kiddopia.SpeedReading.Events;
using Kiddopia.UI;
using MathWhiz.Main.Events;
using Other;
using UnityEngine;
using UnityEngine.UI;

namespace Kiddopia.SpeedReading.Systems
{
    public class SpeedReadingTableSpawnSystem : SystemBase, IListen<RestartGameEvent>
    {
        private DataSpeedReadingTableStarter _tableStarter;
        private GridLayoutGroup _layoutGroup;
        private EntityMono[] _spawnNumbers;
        private Vector2 _cellNumber;

        public override void Activate()
        {
            base.Activate();
            
            _tableStarter = Core.Instance.Get<DataSpeedReadingTableStarter>();
            _tableStarter.SizeTable = LocalStorage.GameData.DataSpeedReading.SizeTable;
            
            this.ListenGlobalEvent<RestartGameEvent>();
            CreateTable(_tableStarter.SizeTable);
        }
    
        public override void Deactivate()
        {
            base.Deactivate();
            this.UnlistenGlobalEvent<RestartGameEvent>();
        }
    
        private void CreateTable(byte sizeOfTable)
        {
            _layoutGroup = ObjectsId.LayoutGroup.SpawnAs<GridLayoutGroup>(_tableStarter.Canvas.transform);
            PreparationLayoutGroup(sizeOfTable, _layoutGroup);

            _spawnNumbers = new EntityMono[sizeOfTable * sizeOfTable];
            
            var randomNumbers = GetRandomNumbers(sizeOfTable);
            
            for (byte i = 0; i < sizeOfTable*sizeOfTable; i++)
            {
                _spawnNumbers[i] = ObjectsId.Number.SpawnAs<EntityMono>(_layoutGroup.transform);
                _spawnNumbers[i].SendEvent(new SpawnNumberEvent()
                {
                    TextNumber = randomNumbers[i].ToString(),
                    ColorBgNumber = _tableStarter.Colors[Random.Range(0, _tableStarter.Colors.Length - 1)]
                });
            }
        }

        private void RestartGame(byte sizeOfTable)
        {
            var randomNumbers = GetRandomNumbers(sizeOfTable);
            
            for (byte i = 0; i < _spawnNumbers.Length; i++)
            {
                _spawnNumbers[i].SendEvent(new MathWhizBorderButtonTransparentEvent());
                _spawnNumbers[i].Get<DataImage>().Value.raycastTarget = true;
                _spawnNumbers[i].Get<DataImage>().Value.color =
                    _tableStarter.Colors[Random.Range(0, _tableStarter.Colors.Length - 1)];
                _spawnNumbers[i].Get<DataTMP>().Value.text = randomNumbers[i].ToString();
            }
        }
    
        private void PreparationLayoutGroup(byte sizeTable, GridLayoutGroup layoutGroup)
        {
            _cellNumber = sizeTable == 7 ? new Vector2(85, 85) : new Vector2(90, 90);
            layoutGroup.cellSize = _cellNumber;
            layoutGroup.constraintCount = sizeTable;
        }

        private static int[] GetRandomNumbers(byte sizeOfTable)
        {
            return Enumerable.Range(1, sizeOfTable*sizeOfTable).Shuffle().ToArray();
        }

        public void handleCallback(RestartGameEvent arguments)
        {
            RestartGame(_tableStarter.SizeTable);
        }
    }
}

