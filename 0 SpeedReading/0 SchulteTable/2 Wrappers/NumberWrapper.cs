﻿using Components;
using Kiddopia.SpeedReading.Behaviours;
using Kiddopia.UI;
using UnityEngine;

namespace Kiddopia.SpeedReading.EntityWrappers
{
    public class NumberWrapper : EntityComponentWrapper
    {
        [SerializeField] private DataTMP tmpData;
        [SerializeField] private DataImage dataImg;
        public override void Link(IComponentHolder componentHolder)
        {
            base.Link(componentHolder);
            componentHolder.Add(tmpData);
            componentHolder.Add(dataImg);
            componentHolder.Add<NumberBehaviour>();
        }
    }
}