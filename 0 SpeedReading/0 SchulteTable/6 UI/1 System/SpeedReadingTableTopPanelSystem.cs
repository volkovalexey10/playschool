﻿using DesertImage.Managers;
using Kiddopia.Audio;
using MathWhiz;
using UI;
using UI.Extensions;

namespace Kiddopia.SpeedReading.UI
{
    public class SpeedReadingTableTopPanelSystem : SystemBase
    {
        private LearningTopPanelSettings _settingsTopPanel;
        private bool _isPlayExercise = false;

        public override void Activate()
        {
            base.Activate();

            _settingsTopPanel = new LearningTopPanelSettings()
            {
                Text = "<size=55>Tap all the numbers in correct order as fast as you can!</size>",
                SoundIds = new[] { (ushort)SoundId.SchulteTablesTask }
            };

            this.ShowScreen(UIIDs.LearningTopPanel, _settingsTopPanel);
        }
    }
}