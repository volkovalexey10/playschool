using System;
using DesertImage;
using DesertImage.Extensions;
using DesertImage.Managers;
using Kiddopia.GameData;
using Kiddopia.SpeedReading.DataComponents;
using Kiddopia.SpeedReading.Events;
using UI;
using UI.Extensions;
using UniRx;

namespace Kiddopia.SpeedReading.UI
{
    
    public class SpeedReadingTableTimerPanelSystem : SystemBase, ITick, IListen<StartTimerEvent>, IListen<StopTimerEvent>,
        IListen<WinEvent>
    {

        private DataSpeedReadingTableStarter _starter;
        private TimerPanelSettings _settings;
        
        private bool _isTimerRunning = false;
        private float _currentTime = 0;
        private float _recordTime;

        public override void Activate()
        {
            base.Activate();
            _starter = Core.Instance.Get<DataSpeedReadingTableStarter>();
            this.ListenGlobalEvent<StartTimerEvent>();
            this.ListenGlobalEvent<StopTimerEvent>();
            this.ListenGlobalEvent<WinEvent>();

            LocalStorage.GameData.DataSpeedReading.SchulteData.TryGetValue(_starter.SizeTable, out _recordTime);
            
            var timeSpan = TimeSpan.FromSeconds(_recordTime);
            
            _settings = new TimerPanelSettings()
            {
                TxtRecordWithNumber = $"{timeSpan.Minutes}:{timeSpan.Seconds}",
                TxtTimerWithNumber = $"{_currentTime}:{_currentTime}",
                CurrentTime = new ReactiveProperty<float>(),
                RecordTime = new ReactiveProperty<float>()
            };
            
            this.ShowScreen(UIIDs.TimerPanel, _settings);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            
            this.UnlistenGlobalEvent<StartTimerEvent>();
            this.UnlistenGlobalEvent<StopTimerEvent>();
            this.UnlistenGlobalEvent<WinEvent>();
        }

        public void handleCallback(StartTimerEvent arguments)
        {
            if(_isTimerRunning) return;
            _isTimerRunning = true;
        }
        public void handleCallback(StopTimerEvent arguments)
        {
            _isTimerRunning = false;
            _settings.CurrentTime.Value = 0f;
            _currentTime = 0f;
        }
        public void handleCallback(WinEvent arguments)
        {
            _isTimerRunning = false;
            _settings.CurrentTime.Value = 0f;
            
            if (_recordTime > 0 && _recordTime < _currentTime) return;
            
            LocalStorage.GameData.DataSpeedReading.SchulteData.Remove(_starter.SizeTable);
            LocalStorage.GameData.DataSpeedReading.SchulteData.Add(_starter.SizeTable,_currentTime);
            _settings.RecordTime.Value = _currentTime;
        }
        public void Tick()
        {
            if(!_isTimerRunning) return;
            _currentTime += UnityEngine.Time.deltaTime;
            _settings.CurrentTime.Value = _currentTime;

        }
    }
    
}
