﻿using DesertImage.Managers;
using Kiddopia.Loading;
using Kiddopia.UI;
using UI;
using UI.Extensions;

namespace Kiddopia.SpeedReading.UI
{
    public class SpeedReadingTableBackPanelSystem : SystemBase
    {

        public override void Activate()
        {
            base.Activate();
            
            this.ShowScreen(UIIDs.BackPanel, new CustomBackPanelSettings
                {OnClick = () => LoadingController.Load((ushort)ScenesId.SpeedReadingSelector)});
            this.ShowScreen(UIIDs.RestartPaintPanel);
        }
    }
}