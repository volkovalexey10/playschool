using System;
using DesertImage.UI.Panel;
using TMPro;
using UI;
using UniRx;

namespace Kiddopia.SpeedReading.UI
{
    public class TimerPanel : Panel<TimerPanelSettings>
    {
        public override ushort Id => (ushort) UIIDs.TimerPanel;
        public TextMeshProUGUI TMProTimer;
        public TextMeshProUGUI TMProRecord;

        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        public override void Setup(TimerPanelSettings settings)
        {
            base.Setup(settings);
            _disposable.Clear();
            settings.CurrentTime
                .Subscribe(value =>
                {
                    var timeSpan = TimeSpan.FromSeconds(value);
                    TMProTimer.text = $"{timeSpan.Minutes}:{timeSpan.Seconds}";
                })
                .AddTo(_disposable);
            settings.RecordTime
                .Subscribe(value =>
                {
                    var timeSpan = TimeSpan.FromSeconds(value);
                    TMProRecord.text = $"{timeSpan.Minutes}:{timeSpan.Seconds}";
                })
                .AddTo(_disposable);
            TMProTimer.text = settings.TxtTimerWithNumber;
            TMProRecord.text = settings.TxtRecordWithNumber;
        }
    }

    public class TimerPanelSettings : IPanelSettings
    {
        public PanelPriority Priority { get; }
        
        public string TxtTimerWithNumber;
        public string TxtRecordWithNumber;
        public ReactiveProperty<float> CurrentTime;
        public ReactiveProperty<float> RecordTime;
    }
}