using Components;
using DesertImage;
using DesertImage.Behaviours;
using Kiddopia.SpeedReading.Events;
using Kiddopia.UI;
using TMPro;
using UnityEngine.UI;

namespace Kiddopia.SpeedReading.Behaviours
{
    public class NumberBehaviour : Behaviour, IListen<SpawnNumberEvent>
    {
        private TextMeshProUGUI _textMeshNumber;
        private Image _imageBg;

        public override void Activate()
        {
            base.Activate();
            _textMeshNumber = Entity.Get<DataTMP>().Value;
            _imageBg = Entity.Get<DataImage>().Value;
            
            Entity.ListenEvent<SpawnNumberEvent>(this);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            Entity.UnlistenEvent<SpawnNumberEvent>(this);
        }

        public void handleCallback(SpawnNumberEvent arguments)
        {
            _textMeshNumber.text = arguments.TextNumber;
            _imageBg.color = arguments.ColorBgNumber;
        }
    }
}