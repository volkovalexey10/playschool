using System;
using Components;
using UnityEngine;

namespace Kiddopia.SpeedReading.DataComponents
{
    [Serializable]
    public class SpeedReadingTableData : DataComponent<SpeedReadingTableData>
    {
        public Canvas Canvas;
        public byte SizeTable;
    }
}


