using System;
using Components;
using UnityEngine;

namespace Kiddopia.SpeedReading.DataComponents
{
    [Serializable]
    public class DataSpeedReadingTableStarter : DataComponent<DataSpeedReadingTableStarter>
    {
        public Canvas Canvas;
        public byte SizeTable;
        public Color32[] Colors;
    }
}


