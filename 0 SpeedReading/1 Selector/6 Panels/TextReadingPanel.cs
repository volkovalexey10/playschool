﻿using System;
using DesertImage;
using DesertImage.UI.Panel;
using Extensions;
using Kiddopia.SpeedReading.Data;
using TMPro;
using UI;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Kiddopia.SpeedReading.Selector
{
    public class TextReadingPanel : Panel<SubmenuSpeedReadingPanelSettings>
    {
        [SerializeField] private TextMeshProUGUI txtTitle;
        [SerializeField] private Transform content;
        [SerializeField] private Button prevBtn;
        [SerializeField] private Button nextBtn;
        
        
        public override ushort Id => (ushort)UIIDs.TextReadingPanelMenu;

        public override PanelPriority Priority => PanelPriority.Master;
        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        public override void Setup(SubmenuSpeedReadingPanelSettings settings)
        {
            base.Setup(settings);
            
            _disposable.Clear();
            
            txtTitle.text = settings.TxtTitle;
            prevBtn.SetOnClick(settings.OnClickPrev);
            nextBtn.SetOnClick(settings.OnClickNext);
            settings.HideNextBtn.Subscribe(value =>
            {
                nextBtn.gameObject.SetActive(!value);
            }).AddTo(_disposable);
            
            settings.HidePrevBtn.Subscribe(value =>
            {
                prevBtn.gameObject.SetActive(!value);
            }).AddTo(_disposable);
        }

        public override void Initialize()
        {
            base.Initialize();

            Core.Instance.Get<DataSelectorStarter>().ContentTextReading = content;
        }
    }

    public class SubmenuSpeedReadingPanelSettings : IPanelSettings
    {
        public PanelPriority Priority { get; }

        public string TxtTitle;
        public Action OnClickNext;
        public Action OnClickPrev;
        public ReactiveProperty<bool> HideNextBtn;
        public ReactiveProperty<bool> HidePrevBtn;
        

    }
}