﻿using DesertImage;
using DesertImage.UI.Panel;
using Kiddopia.SpeedReading.Data;
using TMPro;
using UI;
using UnityEngine;

namespace Kiddopia.SpeedReading.Selector
{
    public class SpeedReadingSelectorPanel : Panel<SpeedReadingSelectorPanelSettings>
    {
        [SerializeField] private TextMeshProUGUI txtTitle;
        [SerializeField] private Transform content;
        
        public override ushort Id => (ushort)UIIDs.SelectorPanel;

        public override PanelPriority Priority => PanelPriority.Master;
        public override void Setup(SpeedReadingSelectorPanelSettings settings)
        {
            base.Setup(settings);

            txtTitle.text = settings.TxtTitle;
        }

        public override void Initialize()
        {
            base.Initialize();

            Core.Instance.Get<DataSelectorStarter>().ContentGameMode = content;
        }
    }

    public class SpeedReadingSelectorPanelSettings : IPanelSettings
    {
        public PanelPriority Priority { get; }

        public string TxtTitle;

    }
}