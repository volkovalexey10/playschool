﻿using DesertImage;
using DesertImage.UI.Panel;
using Kiddopia.SpeedReading.Data;
using TMPro;
using UI;
using UnityEngine;

namespace Kiddopia.SpeedReading.Selector
{
    public class SchultePanel : Panel<SubmenuSpeedReadingPanelSettings>
    {
        [SerializeField] private TextMeshProUGUI txtTitle;
        [SerializeField] private Transform content;
        
        public override ushort Id => (ushort)UIIDs.SchultePanelMenu;

        public override PanelPriority Priority => PanelPriority.Master;
        public override void Setup(SubmenuSpeedReadingPanelSettings settings)
        {
            base.Setup(settings);

            txtTitle.text = settings.TxtTitle;
            
        }

        public override void Initialize()
        {
            base.Initialize();

            Core.Instance.Get<DataSelectorStarter>().ContentSchulte = content;
        }
    }
}