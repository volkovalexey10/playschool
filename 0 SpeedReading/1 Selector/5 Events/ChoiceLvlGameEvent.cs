﻿using Kiddopia.Loading;

namespace Kiddopia.SpeedReading.Events
{
    public struct ChoiceLvlGameEvent
    {
        public ScenesId ScenesId;
        public byte CurrentText;
        public byte SizeTable;
    }
}