﻿using Kiddopia.Loading;

namespace Kiddopia.SpeedReading.Events
{
    public struct ChoiceGameMode
    {
        public ScenesId ScenesId;
    }
}