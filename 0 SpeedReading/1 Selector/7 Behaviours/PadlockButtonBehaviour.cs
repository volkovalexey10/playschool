﻿using DesertImage;
using DesertImage.Behaviours;
using DesertImage.Extensions;
using Interaction;
using Kiddopia.CapableKids.Events;
using Kiddopia.Loading;
using Kiddopia.SpeedReading.Events;
using Kiddopia.UI;
using Subscription.UI;
using UI;
using UI.Extensions;

namespace Kiddopia.SpeedReading.Behaviours
{
    public class PadlockButtonBehaviour : Behaviour, IListen<ClickedEvent>
    {
        public ScenesId ScenesId;
            public override void Activate()
        {
            base.Activate();
            Entity.ListenEvent<ClickedEvent>(this);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            Entity.UnlistenEvent<ClickedEvent>(this);
        }

        public void handleCallback(ClickedEvent arguments)
        {
            ShowPromoWindow();
            this.HideScreen(UIIDs.BackPanel);
            this.HideScreen(UIIDs.SelectorPanel);
            this.HideScreen(UIIDs.SchultePanelMenu);
            this.HideScreen(UIIDs.TextReadingPanelMenu);
            
        }
        
        private void ShowPromoWindow()
        {
            this.ShowScreen
            (
                UIIDs.SubscribePromoWindow,
                new SubscriptionPromoWindowSettings
                {
                    OnCloseAction = () =>
                    {
                        this.HideScreen(UIIDs.SubscribePromoWindow);
                        this.ShowScreen(UIIDs.BackPanel, new CustomBackPanelSettings
                        {
                            OnClick = () =>
                            {
                                this.HideScreen(UIIDs.SchultePanelMenu);
                                this.HideScreen(UIIDs.TextReadingPanelMenu);
                                this.HideScreen(UIIDs.SubscribePromoWindow);
                                this.HideScreen(UIIDs.BackPanel);
                                this.ShowScreen(UIIDs.SelectorPanel);
                                this.ShowScreen(UIIDs.BackPanel);
                            }
                        });
                        this.ShowScreen(ScenesId == ScenesId.SpeedReadingSchulteTable
                            ? UIIDs.SchultePanelMenu
                            : UIIDs.TextReadingPanelMenu);
                    },
                    OnMainAction = () =>
                    {
                        this.ShowScreen
                        (
                            UIIDs.SubscriptionPlanWindow,
                            new SubscriptionPlanWindowSettings
                            {
                                OnBackAction = ShowPromoWindow,
                                OnCloseAction = () =>
                                {
                                    this.HideScreen(UIIDs.SubscriptionPlanWindow);
                                    this.ShowScreen(UIIDs.BackPanel, new CustomBackPanelSettings
                                    {
                                        OnClick = () =>
                                        {
                                            this.HideScreen(UIIDs.SchultePanelMenu);
                                            this.HideScreen(UIIDs.TextReadingPanelMenu);
                                            this.HideScreen(UIIDs.SubscribePromoWindow);
                                            this.HideScreen(UIIDs.BackPanel);
                                            this.ShowScreen(UIIDs.SelectorPanel);
                                            this.ShowScreen(UIIDs.BackPanel);
                                        }
                                    });
                                    this.ShowScreen(ScenesId == ScenesId.SpeedReadingSchulteTable
                                        ? UIIDs.SchultePanelMenu
                                        : UIIDs.TextReadingPanelMenu);
                                },
                                OnSuccessCallback = () =>
                                {
                                    this.HideScreen(UIIDs.BuyItemPopup);
                                    this.SendGlobalEvent(new OpenFullVersionSpeedReadingEvent());
                                }
                            }
                        );
                    }
                }
            );
        }
    }
}