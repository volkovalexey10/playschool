﻿using DesertImage;
using DesertImage.Behaviours;
using DesertImage.Extensions;
using Game.Vibration;
using Interaction;
using Kiddopia.SpeedReading.Data;
using Kiddopia.SpeedReading.Events;

namespace Kiddopia.SpeedReading.Behaviours
{
    public class GameModeBehaviour : Behaviour, IListen<ClickedEvent>
    {
        public override void Activate()
        {
            base.Activate();
            
            Entity.ListenEvent<ClickedEvent>(this);
        }

        public override void Deactivate()
        {
            base.Deactivate();
            Entity.UnlistenEvent<ClickedEvent>(this);
        }

        public void handleCallback(ClickedEvent arguments)
        {
            this.SendGlobalEvent(new VibrationSingleEvent());
            this.SendGlobalEvent(new ChoiceGameMode
            {
                ScenesId = arguments.Value.Get<DataGameMode>().ScenesId
            });
        }
    }
}