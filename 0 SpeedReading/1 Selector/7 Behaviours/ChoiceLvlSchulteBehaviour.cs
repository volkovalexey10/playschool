﻿using DesertImage.Behaviours;
using DesertImage.Extensions;
using Extensions;
using Game.Vibration;
using Kiddopia.SpeedReading.Data;
using Kiddopia.SpeedReading.Events;

namespace Kiddopia.SpeedReading.Behaviours
{
    public class ChoiceLvlSchulteBehaviour : Behaviour
    {
        private DataChoiceLvlSchulte _dataIcon;
        public override void Activate()
        {
            base.Activate();
            
            _dataIcon = Entity.Get<DataChoiceLvlSchulte>();
            _dataIcon.Button.SetOnClick(SetButton);
        }

        private void SetButton()
        {
            this.SendGlobalEvent(new VibrationSingleEvent());
            this.SendGlobalEvent(new ChoiceLvlGameEvent
            {
                ScenesId = _dataIcon.ScenesId,
                SizeTable = _dataIcon.SizeTable
            });
        }
    }
}