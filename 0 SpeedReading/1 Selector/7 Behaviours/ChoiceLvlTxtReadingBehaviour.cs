﻿using DesertImage.Behaviours;
using DesertImage.Extensions;
using Extensions;
using Game.Vibration;
using Kiddopia.SpeedReading.Data;
using Kiddopia.SpeedReading.Events;

namespace Kiddopia.SpeedReading.Behaviours
{
    public class ChoiceLvlTxtReadingBehaviour : Behaviour
    {
        private DataChoiceLvlTxtReading _dataButton;
        public override void Activate()
        {
            base.Activate();
            _dataButton = Entity.Get<DataChoiceLvlTxtReading>();
            _dataButton.Button.SetOnClick(SetButton);
        }


        private void SetButton()
        {
            this.SendGlobalEvent(new VibrationSingleEvent());
            this.SendGlobalEvent(new ChoiceLvlGameEvent
            {
                ScenesId = _dataButton.ScenesId,
                CurrentText = _dataButton.CurrentText
            });
        }
    }
}