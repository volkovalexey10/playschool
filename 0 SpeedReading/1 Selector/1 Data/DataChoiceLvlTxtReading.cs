﻿using System;
using Components;
using Kiddopia.Loading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Kiddopia.SpeedReading.Data
{
    [Serializable]
    public class DataChoiceLvlTxtReading : DataComponent<DataChoiceLvlTxtReading>
    {
        public TextMeshProUGUI TxtTitle;
        public TextMeshProUGUI TxtDescription;
        public Transform LayoutStars;
        public GameObject Cup;
        public ScenesId ScenesId;
        public byte CurrentText;
        public GameObject Padlock;
        public Button Button;

    }
}