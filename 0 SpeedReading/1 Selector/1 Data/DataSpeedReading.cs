﻿using System;
using External;

namespace Kiddopia.SpeedReading.Data
{
    [Serializable]
    public struct DataSpeedReading
    {
        public CustomDictionary<byte, float> SchulteData;
        public CustomDictionary<byte, bool> TextReadingData;
        public byte CurrentText;
        public byte SizeTable;
        public SpeedReadingPanel PrevPanel;
    }

    public enum SpeedReadingPanel
    {
        
        GameModes = 0,
        Schulte,
        Reading,
    }
}