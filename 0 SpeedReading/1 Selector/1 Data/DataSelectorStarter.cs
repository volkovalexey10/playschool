﻿using System;
using Components;
using Kiddopia.Loading;
using UnityEngine;

namespace Kiddopia.SpeedReading.Data
{
    [Serializable]
    public class DataSelectorStarter : DataComponent<DataSelectorStarter>
    {
        [HideInInspector] public Transform ContentGameMode;
        [HideInInspector] public Transform ContentSchulte;
        [HideInInspector] public Transform ContentTextReading;
        public DataSpeedReadingIcon[] GameModeIconDatas;
        public DataSchulteIcon[] SchulteIconDatas;
        public DataTxtReadingIcon[] TxtReadingIconDatas;
        
    }
    
    [Serializable]
    public struct DataSpeedReadingIcon
    {
        public string TxtTitle;
        public string TxtDescription;
        public Sprite SpriteIcon;
        public ScenesId ScenesId;
        
    }

    [Serializable]
    public struct DataSchulteIcon
    {
        public string TxtTitle;
        public Sprite SpriteIcon;
        public Vector3 ScaleIcon;
        public ScenesId ScenesId;
        public byte SizeTable;

    }
    [Serializable]
    public struct DataTxtReadingIcon
    {
        public string TxtTitle;
        public string TxtDescription;
        public ScenesId ScenesId;
        public byte CurrentText;

    }
}