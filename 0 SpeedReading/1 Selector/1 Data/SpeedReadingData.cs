﻿using System;

namespace Kiddopia.SpeedReading.Data
{
    [Serializable]
    public struct SpeedReadingData
    {
        public float RecordTime;
        public byte[] StarRecordsText;

    }
}