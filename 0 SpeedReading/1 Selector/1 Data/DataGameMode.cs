﻿using System;
using Components;
using Kiddopia.Loading;
using TMPro;
using UnityEngine.UI;

namespace Kiddopia.SpeedReading.Data
{
    [Serializable]
    public class DataGameMode : DataComponent<DataGameMode>
    {
        public TextMeshProUGUI TxtTitle;
        public TextMeshProUGUI TxtDescription;
        public TextMeshProUGUI TxtSlider;
        public Image ImageIcon;
        public Slider Slider;
        public ScenesId ScenesId;

    }
}