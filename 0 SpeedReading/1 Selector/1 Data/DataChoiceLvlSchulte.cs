﻿using System;
using Components;
using Kiddopia.Loading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Kiddopia.SpeedReading.Data
{
    [Serializable]
    public class DataChoiceLvlSchulte : DataComponent<DataChoiceLvlSchulte>
    {
        public TextMeshProUGUI TxtTitle;
        public TextMeshProUGUI TxtRecord;
        public Transform IconTransform;
        public Image ImageIcon;
        public ScenesId ScenesId;
        public byte SizeTable;
        public GameObject Padlock;
        public Button Button;

    }
}