﻿using System;
using Components;
using UnityEngine;

namespace Kiddopia.SpeedReading.Data
{
    [Serializable]
    public class SpeedReadingSelectorData : DataComponent<SpeedReadingSelectorData>
    {
        public Canvas CanvasMain;
        public Canvas CanvasSchulteTable;
        public Canvas CanvasReadingText;
    }
}