﻿using Components;
using Kiddopia.Loading;
using Kiddopia.SpeedReading.Behaviours;
using UnityEngine;

namespace Kiddopia.SpeedReading.Wrappers
{
    public class PadlockButtonWrapper : EntityComponentWrapper
    {
        [SerializeField] private ScenesId scenesId;
        public override void Link(IComponentHolder componentHolder)
        {
            base.Link(componentHolder);
            componentHolder.Add<PadlockButtonBehaviour>().ScenesId = scenesId;
        }
    }
}