﻿using Components;
using Kiddopia.SpeedReading.Behaviours;
using Kiddopia.SpeedReading.Data;
using UnityEngine;

namespace Kiddopia.SpeedReading.Wrappers
{
    public class GameModeWrapper : EntityComponentWrapper
    {
        [SerializeField] private DataGameMode prefab;
        public override void Link(IComponentHolder componentHolder)
        {
            base.Link(componentHolder);
            componentHolder.Add(prefab);
            componentHolder.Add<GameModeBehaviour>();
        }
    }
}