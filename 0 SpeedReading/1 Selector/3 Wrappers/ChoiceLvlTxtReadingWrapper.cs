﻿using Components;
using Kiddopia.SpeedReading.Behaviours;
using Kiddopia.SpeedReading.Data;
using UnityEngine;

namespace Kiddopia.SpeedReading.Wrappers
{
    public class ChoiceLvlTxtReadingWrapper : EntityComponentWrapper
    {
        [SerializeField] private DataChoiceLvlTxtReading lvl;
        public override void Link(IComponentHolder componentHolder)
        {
            base.Link(componentHolder);
            componentHolder.Add(lvl);
            componentHolder.Add<ChoiceLvlTxtReadingBehaviour>();
        }
    }
}