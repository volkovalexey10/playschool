﻿using Components;
using Kiddopia.SpeedReading.Behaviours;
using Kiddopia.SpeedReading.Data;
using UnityEngine;

namespace Kiddopia.SpeedReading.Wrappers
{
    public class ChoiceLvlSchulteWrapper : EntityComponentWrapper
    {
        [SerializeField] private DataChoiceLvlSchulte lvlSchulte;
        public override void Link(IComponentHolder componentHolder)
        {
            base.Link(componentHolder);
            componentHolder.Add(lvlSchulte);
            componentHolder.Add<ChoiceLvlSchulteBehaviour>();
        }
    }
}