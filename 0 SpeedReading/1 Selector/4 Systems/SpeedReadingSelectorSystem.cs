﻿using System;
using System.Collections.Generic;
using DesertImage;
using DesertImage.Entities;
using DesertImage.Extensions;
using DesertImage.Managers;
using Entities;
using External;
using Game;
using Kiddopia.Audio;
using Kiddopia.GameData;
using Kiddopia.Loading;
using Kiddopia.Spawning;
using Kiddopia.SpeedReading.Data;
using Kiddopia.SpeedReading.Events;
using Kiddopia.SpeedReading.Selector;
using Kiddopia.UI;
using UI;
using UI.Extensions;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using DataGameMode = Kiddopia.SpeedReading.Data.DataGameMode;

namespace Kiddopia.SpeedReading.Systems
{
    public enum OpenPanel
    {
        Schulte,
        Reading
    }
    public class SpeedReadingSelectorSystem : SystemBase, IListen<ChoiceGameMode>, IListen<ChoiceLvlGameEvent>, IListen<OpenFullVersionSpeedReadingEvent>
    {
        private DataSelectorStarter _dataStarter;
        private List<GameObject> _iconContainer;
        private List<DataChoiceLvlSchulte> _dataSchulteIcon;
        private List<DataChoiceLvlTxtReading> _dataReadingIcon;
        private SubmenuSpeedReadingPanelSettings _submenuSettings;
        private sbyte _currentContainer;
        private const byte _minCountIcon = 3;
        public override void Activate()
        {
            base.Activate();
            LocalStorage.GameData.DataSpeedReading.SchulteData ??= new CustomDictionary<byte, float>();
            LocalStorage.GameData.DataSpeedReading.TextReadingData ??= new CustomDictionary<byte, bool>();
            
            _currentContainer = 1;
            _dataSchulteIcon = new List<DataChoiceLvlSchulte>();
            _dataReadingIcon = new List<DataChoiceLvlTxtReading>();
            _iconContainer = new List<GameObject>();
            _dataStarter = Core.Instance.Get<DataSelectorStarter>();
            
            if (_dataStarter.TxtReadingIconDatas.Length > _minCountIcon)
            {
                _submenuSettings = new SubmenuSpeedReadingPanelSettings
                {
                    HideNextBtn = new ReactiveProperty<bool>(),
                    HidePrevBtn = new ReactiveProperty<bool>(true)
                };
            }
            else
            {
                _submenuSettings = new SubmenuSpeedReadingPanelSettings
                {
                    HideNextBtn = new ReactiveProperty<bool>(true),
                    HidePrevBtn = new ReactiveProperty<bool>(true)
                };
            }
            
            this.ListenGlobalEvent<ChoiceGameMode>();
            this.ListenGlobalEvent<ChoiceLvlGameEvent>();
            this.ListenGlobalEvent<OpenFullVersionSpeedReadingEvent>();
            
            CreateGameModeIcons();
            CreateIconSchulte();
            CreateIconReading();
            ShowPanel();
        }

        public override void Deactivate()
        {
            this.UnlistenGlobalEvent<ChoiceGameMode>();
            this.UnlistenGlobalEvent<ChoiceLvlGameEvent>();
            this.UnlistenGlobalEvent<OpenFullVersionSpeedReadingEvent>();
            
            var indexBackToMenu = LocalStorage.MenuNavigationData.CurrentData.Items.Length - 1;
            
            LocalStorage.MenuNavigationData.CurrentData.Index = indexBackToMenu;
            base.Deactivate();
        }

        public void handleCallback(OpenFullVersionSpeedReadingEvent arguments)
        {
            foreach (var icon in _dataReadingIcon)
            {
                icon.Padlock.SetActive(!LocalStorage.IsFullVersion);
            }

            foreach (var icon in _dataSchulteIcon)
            {
                icon.Padlock.SetActive(!LocalStorage.IsFullVersion);
            }
        }
        
        public void handleCallback(ChoiceLvlGameEvent arguments)
        {
            var prevPanel = SpeedReadingPanel.Reading;
            
            switch (arguments.ScenesId)
            {
                case ScenesId.SpeedReadingSchulteTable:
                    LocalStorage.GameData.DataSpeedReading.SizeTable = arguments.SizeTable;
                    prevPanel = SpeedReadingPanel.Schulte;
                    break;
                case ScenesId.SpeedReadingTextReading:
                    LocalStorage.GameData.DataSpeedReading.CurrentText = arguments.CurrentText;
                    break;
            }

            LocalStorage.GameData.DataSpeedReading.PrevPanel = prevPanel;
            LoadingController.Load((ushort) arguments.ScenesId);
        }

        public void handleCallback(ChoiceGameMode arguments)
        {
            ShowSubmenu(arguments.ScenesId);
        }

        
        private void ShowSubmenu(ScenesId scenesId)
        {
            LocalStorage.GameData.DataSpeedReading.PrevPanel = SpeedReadingPanel.GameModes;
            
            var sound = SoundId.SchulteTables.Play2D(SoundPriority.Important);
            var textPanel = "<color=#E80060>SCHULTE TABLES</color>";
            var numberPanel = UIIDs.SchultePanelMenu;
            
            if (scenesId == ScenesId.SpeedReadingTextReading)
            {
                sound = SoundId.TextReading.Play2D(SoundPriority.Important);
                textPanel = "<color=#E80060>TEXT READING</color>";
                numberPanel = UIIDs.TextReadingPanelMenu;
            }

            _submenuSettings.TxtTitle = textPanel;
            _submenuSettings.OnClickNext = () => NextPrevContainer(true);
            _submenuSettings.OnClickPrev = () => NextPrevContainer(false);
            
            this.DoActionWithDelay(() =>
            {
                this.HideScreen(UIIDs.BackPanel);
                this.HideScreen(UIIDs.SelectorPanel);
                
                this.ShowScreen(numberPanel, _submenuSettings);
                this.ShowScreen(UIIDs.BackPanel, new CustomBackPanelSettings
                {
                    OnClick = ShowPanel
                });
            }, sound.Delay);

        }

        private void CreateGameModeIcons()
        {
            const byte maxIcon = 2;
            var countIcon = 0;
            var maxValueSlider = 0;
            var speedReadingData = LocalStorage.GameData.DataSpeedReading;
            var countSpawnLayout = _dataStarter.GameModeIconDatas.Length / maxIcon;
            
            if (_dataStarter.GameModeIconDatas.Length % maxIcon != 0)
            {
                countSpawnLayout++;
            }

            while(countSpawnLayout != 0)
            {
                var objLayout = ObjectsId.LayoutGroup.SpawnAs<GridLayoutGroup>(_dataStarter.ContentGameMode);
                objLayout.gameObject.SetActive(false);
                _iconContainer.Add(objLayout.gameObject);
                var countSpawnIcon = 0;

                for (; countIcon < _dataStarter.GameModeIconDatas.Length; countIcon++)
                {
                    if (countSpawnIcon >= maxIcon) break;

                    var iconData = _dataStarter.GameModeIconDatas[countIcon];
                    var valueSlider = 0;
                    switch (iconData.ScenesId)
                    {
                        case ScenesId.SpeedReadingSchulteTable:
                            maxValueSlider = _dataStarter.SchulteIconDatas.Length;
                            valueSlider = speedReadingData.SchulteData.Count;
                            break;
                        case ScenesId.SpeedReadingTextReading:
                            maxValueSlider = _dataStarter.TxtReadingIconDatas.Length;
                            valueSlider += speedReadingData.TextReadingData.Count;
                            break;
                    }

                    var objGameMode = ObjectsId.SelectorGameMode.SpawnAs<EntityMono>(objLayout.transform);
                    var dataGameMode = objGameMode.Get<DataGameMode>();

                    dataGameMode.TxtTitle.text = iconData.TxtTitle;
                    dataGameMode.TxtDescription.text = iconData.TxtDescription;
                    dataGameMode.TxtSlider.text = $"{valueSlider}/{maxValueSlider}";
                    dataGameMode.Slider.maxValue = maxValueSlider;
                    dataGameMode.Slider.value = valueSlider;
                    dataGameMode.ImageIcon.sprite = iconData.SpriteIcon;
                    dataGameMode.ScenesId = iconData.ScenesId;

                    countSpawnIcon++;
                    
                }
                countSpawnLayout--;
            }

            _iconContainer[0].SetActive(true);
            
        }
        private void CreateIconSchulte()
        {
            _iconContainer.Clear();
            const byte maxIcon = 3;
            var countIcon = 0;
            var countSpawnLayout = _dataStarter.SchulteIconDatas.Length / maxIcon;
            
            if (_dataStarter.SchulteIconDatas.Length % maxIcon != 0)
            {
                countSpawnLayout++;
            }
            while(countSpawnLayout != 0)
            {
                var objLayout = ObjectsId.LayoutGroup.SpawnAs<GridLayoutGroup>(_dataStarter.ContentSchulte);
                objLayout.spacing = new Vector2(-180, 0);
                objLayout.gameObject.SetActive(false);
                _iconContainer.Add(objLayout.gameObject);
                var countSpawnIcon = 0;
                
                for (;countIcon < _dataStarter.SchulteIconDatas.Length; countIcon++)
                {
                    if (countSpawnIcon >= maxIcon) break;
                    var iconData = _dataStarter.SchulteIconDatas[countIcon];
                    LocalStorage.GameData.DataSpeedReading.SchulteData.TryGetValue(iconData.SizeTable, out var seconds);
                    var objSchulte = ObjectsId.SchulteIconSelector.SpawnAs<EntityMono>(objLayout.transform);
                    var objData = objSchulte.Get<DataChoiceLvlSchulte>();
                    _dataSchulteIcon.Add(objData);
                    var time = TimeSpan.FromSeconds(seconds);
                    
                    objData.ImageIcon.sprite = iconData.SpriteIcon;
                    objData.ScenesId = iconData.ScenesId;
                    objData.IconTransform.localScale = iconData.ScaleIcon;
                    objData.SizeTable = iconData.SizeTable;
                    objData.TxtTitle.text = iconData.TxtTitle;
                    objData.TxtRecord.text = $"{time.Minutes}:{time.Seconds}";
                    if (countIcon >= 1)
                    {
                        objData.Padlock.SetActive(!LocalStorage.IsFullVersion);
                    }
                    else
                    {
                        objData.Padlock.SetActive(false);
                    }
                        
                    countSpawnIcon++;
                }
                countSpawnLayout--;;
            }

            _iconContainer[0].gameObject.SetActive(true);
        }
        private void CreateIconReading()
        {
            const byte maxIcon = 3;
            var countIcon = 0;
            var countStars = 1;
            var countSpawnLayout = _dataStarter.TxtReadingIconDatas.Length / maxIcon;
            
            if (_dataStarter.SchulteIconDatas.Length % maxIcon != 0)
            {
                countSpawnLayout++;
            }

            while (countSpawnLayout != 0)
            {
                var objLayout = ObjectsId.LayoutGroup.SpawnAs<GridLayoutGroup>(_dataStarter.ContentTextReading);
                objLayout.padding.top = 50;
                objLayout.spacing = new Vector2(-200, 0);
                objLayout.gameObject.SetActive(false);
                _iconContainer.Add(objLayout.gameObject);
                var countSpawnIcon = 0;
                
                for (;countIcon < _dataStarter.TxtReadingIconDatas.Length; countIcon++)
                {
                    if (countSpawnIcon >= maxIcon) break;
                    var iconData = _dataStarter.TxtReadingIconDatas[countIcon];
                    LocalStorage.GameData.DataSpeedReading.TextReadingData.TryGetValue(
                        iconData.CurrentText, out var isComplete);
                    var objReading = ObjectsId.TxtReadingIconSelector.SpawnAs<EntityMono>(objLayout.transform);
                    var objData = objReading.Get<DataChoiceLvlTxtReading>();
                    _dataReadingIcon.Add(objData);
                    
                    for (byte i = 0; i < countStars; i++)
                        ObjectsId.Star.SpawnAs<EntityMono>(objData.LayoutStars);

                    objData.TxtTitle.text = iconData.TxtTitle;
                    objData.TxtDescription.text = iconData.TxtDescription;
                    objData.ScenesId = iconData.ScenesId;
                    objData.Cup.SetActive(isComplete);
                    objData.CurrentText = iconData.CurrentText;
                    
                    if (countIcon >= 3)
                    {
                        objData.Padlock.SetActive(!LocalStorage.IsFullVersion);
                    }
                    else
                    {
                        objData.Padlock.SetActive(false);
                    }
                    countSpawnIcon++;
                    if (countStars >= 3) continue;
                    countStars++;
                }
                countSpawnLayout--;
            }

            _iconContainer[1].gameObject.SetActive(true);
        }

        private void NextPrevContainer(bool isNext)
        {
            _iconContainer[_currentContainer].SetActive(false);
            if (isNext)
                _currentContainer++;
            else
                _currentContainer--;
            _submenuSettings.HideNextBtn.Value = _currentContainer == _iconContainer.Count - 1;
            _submenuSettings.HidePrevBtn.Value = _currentContainer == 1;
            
            _iconContainer[_currentContainer].SetActive(true);
        }

        private void ShowPanel()
        {
            var currentPanel = LocalStorage.GameData.DataSpeedReading.PrevPanel;
            
            this.HideScreen(UIIDs.SchultePanelMenu);
            this.HideScreen(UIIDs.SelectorPanel);
            this.HideScreen(UIIDs.TextReadingPanelMenu);
            this.HideScreen(UIIDs.SubscribePromoWindow);
            this.HideScreen(UIIDs.BackPanel);
            
            switch (currentPanel)
            {
                case SpeedReadingPanel.GameModes:
                    this.ShowScreen(UIIDs.SelectorPanel, new SpeedReadingSelectorPanelSettings {TxtTitle = "<color=#00A8FF>SPEED READING</color>"});
                    this.ShowScreen(UIIDs.BackPanel, new CustomBackPanelSettings{OnClick = ()=> LoadingController.Load((ushort)ScenesId.Menu)});
                    return;
                
                case SpeedReadingPanel.Schulte:
                    ShowSubmenu(ScenesId.SpeedReadingSchulteTable);
                    return;
                
                case SpeedReadingPanel.Reading:
                    ShowSubmenu(ScenesId.SpeedReadingTextReading);
                    return;
            }
        }
    }
}
