﻿using System.Collections;
using DesertImage.Starters;
using Game;
using GameInput;
using Kiddopia.UI;
using Kiddopia.GameData;
using UnityEngine;
using DesertImage.Audio;
using Dots.Systems;
using Game.Vibration;
using Kiddopia.AgeConfirm.UI;
using Kiddopia.Analytics;
using Kiddopia.Audio;
using Kiddopia.Economic;
using Kiddopia.IAP;
using Kiddopia.Reward;
using Kiddopia.SpeedReading.Data;
using Kiddopia.SpeedReading.Systems;
using Statistic;
using Subscription;
using Vibration.DataComponents;
using DataGameMode = Game.DataGameMode;

namespace Kiddopia.Starters
{
    public class SpeedReadingStarterSelector : Starter
    {

        protected override IEnumerator InitData()
        {
            var process = base.InitData();

            while (process.MoveNext())
            {
                yield return null;
            }

            Core.Add<DataGameMode>().Value = GameModes.SpeedReading;

            Core.Add<DataUIManager>();
            
            Core.Add<DataVibration>();
            Core.Add<DataSoftMoney>();
            Core.Add<DataHardMoney>();
        }

        protected override IEnumerator InitSystems()
        {
            var process = base.InitSystems();

            while (process.MoveNext())
            {
                yield return null;
            }

            var _selectorData = Core.Get<DataSelectorStarter>();
            while (_selectorData == null)
            {
                _selectorData = Core.Get<DataSelectorStarter>();
            }

            yield return Resources.UnloadUnusedAssets();

            Core.Add<SaveLoadSystem>();
            Core.Add<AutoSaveSystem>();

            StatisticPack.Integrate(Core);
            RewardStuffPack.AddTo(Core);
            
            Core.Add<IAPInitializeSystem>();
            Core.Add<IAPBuySystem>();
            
            Core.Add<SubscriptionInitSystem>();
            Core.Add<SubscriptionManagingSystem>();
            
            Core.Add<EconomicMoneyManagingSystem>();
            Core.Add<EconomicMoneySaveLoadSystem>();
            
            Core.Add<UIControllerSystem>();
            
            Core.Add<UIAgeConfirmSystem>();
            
            Core.Add<SoundPlayWhenClickEmptySpaceSystem2D>();
            
            Core.Add<InputStandaloneSystem>();
            
            Core.Add<VibrationHapticSystem>();
            
            Core.Add<MusicSystem>();
            Core.Add<LevelCompleteSystem>(); // why? 
            
            Core.Add<SpeedReadingSelectorSystem>();

            // Amplitude analytics
            Core.Add<AmplitudeSystem>();
            Core.Add<AmplitudeIAPEventsSupervisor>();
            //Core.Add<AmplitudeLevelCompleteSupervisor>(); // not used.
            Core.Add<AmplitudeSpeedReadingSelectorSupervisor>();
        }

        protected override IEnumerator InitManagers()
        {
            var process = base.InitManagers();

            while (process.MoveNext())
            {
                yield return null;
            }

            Core.Add<MusicPlayer>();
        }
    }
}