﻿using Framework.Components;
using UnityEngine;

namespace Kiddopia.SpeedReading.DataComponents
{
    public class TextReadingDataTextWrapper : ComponentWrapper<DataTextReadingQuiz>
    {
        [SerializeField] private ScriptableSpeedReadingTextReading[] Texts;
        [SerializeField] private ScriptableSpeedReadingQuiz[] QuizMary;
        [SerializeField] private ScriptableSpeedReadingQuiz[] QuizTheWonderful;
        [SerializeField] private ScriptableSpeedReadingQuiz[] QuizTheSecretGarden;

        protected override DataTextReadingQuiz GetData()
        {
            data.Texts = Texts;
            data.QuizMary = QuizMary;
            data.QuizTheWonderful = QuizTheWonderful;
            data.QuestionsTheSecretGarden = QuizTheSecretGarden;
            
            return base.GetData();
        }
    }
}