﻿using Components;
using UnityEngine;

namespace Kiddopia.SpeedReading.Wrappers
{
    public class TextWrapper : EntityComponentWrapper
    {
        [SerializeField] private DataTMP data;
        public override void Link(IComponentHolder componentHolder)
        {
            base.Link(componentHolder);
            componentHolder.Add(data);
        }
    }
}