﻿using Components;
using Kiddopia.SpeedReading.Behaviours;
using Kiddopia.SpeedReading.DataComponents;
using Kiddopia.UI;
using UnityEngine;

namespace Kiddopia.SpeedReading.EntityWrappers
{
    public class ButtonQuizDataWrapper : EntityComponentWrapper
    {
        [SerializeField] private DataTMP dataTMP;
        [SerializeField] private DataImage dataImage;
        [SerializeField] private DataQuizButton dataQuizButton;
        public override void Link(IComponentHolder componentHolder)
        {
            base.Link(componentHolder);
            componentHolder.Add(dataTMP);
            componentHolder.Add(dataImage);
            componentHolder.Add(dataQuizButton);
            componentHolder.Add<ButtonQuizBehaviour>();

        }
    }
}