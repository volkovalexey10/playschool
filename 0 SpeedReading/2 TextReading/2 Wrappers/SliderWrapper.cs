﻿using Components;
using Kiddopia.DataComponents;
using UnityEngine;

namespace Kiddopia.SpeedReading.EntityWrappers
{
    public class SliderWrapper : EntityComponentWrapper
    {
        [SerializeField] private DataSlider data;
        public override void Link(IComponentHolder componentHolder)
        {
            base.Link(componentHolder);
            componentHolder.Add(data);
        }
    }
}