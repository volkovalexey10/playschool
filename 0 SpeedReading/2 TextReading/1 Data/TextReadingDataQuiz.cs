﻿using System;
using System.Collections.Generic;
using Components;

namespace Kiddopia.SpeedReading.DataComponents
{
    [Serializable]
    public class TextReadingDataQuiz : DataComponent<TextReadingDataQuiz>
    {
        public ISpeedReadingText[] Texts;
        public IQuiz[] QuizMary;
        public IQuiz[] QuizTheWonderful;
        public IQuiz[] QuestionsTheSecretGarden;

    }
}