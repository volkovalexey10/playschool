﻿using System;
using Components;
using UnityEngine;
using UnityEngine.UI;

namespace Kiddopia.SpeedReading.DataComponents
{
    [Serializable]
    public class TextReadingStarterData : DataComponent<TextReadingStarterData>
    {
        public Canvas Canvas;
        public float SpeedText;
        public byte CurrentText;
        public byte CurrentQuiz;
    }
}