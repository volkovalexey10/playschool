﻿using System;
using Components;
using UnityEngine;

namespace Kiddopia.SpeedReading.DataComponents
{
    [Serializable]
    public class DataQuizButton : DataComponent<DataQuizButton>
    {
        public Sprite[] Borders;
    }
}