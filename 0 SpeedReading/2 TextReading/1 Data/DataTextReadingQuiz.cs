﻿using System;
using Components;

namespace Kiddopia.SpeedReading.DataComponents
{
    [Serializable]
    public class DataTextReadingQuiz : DataComponent<DataTextReadingQuiz>
    {
        public ISpeedReadingText[] Texts;
        public IQuiz[] QuizMary;
        public IQuiz[] QuizTheWonderful;
        public IQuiz[] QuestionsTheSecretGarden;

    }
}