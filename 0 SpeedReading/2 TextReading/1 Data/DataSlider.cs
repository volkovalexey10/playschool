﻿using Components;
using System;
using UnityEngine.UI;

namespace Kiddopia.DataComponents
{
    [Serializable]
    public class DataSlider : DataComponent<DataSlider>
    {
        public Slider Value;

    }
}