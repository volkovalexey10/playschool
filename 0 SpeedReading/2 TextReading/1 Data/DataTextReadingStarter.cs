﻿using System;
using Components;

namespace Kiddopia.SpeedReading.DataComponents
{
    [Serializable]
    public class DataTextReadingStarter : DataComponent<DataTextReadingStarter>
    {
        public byte CurrentText;
        public bool IsTapAnswer;
    }
}