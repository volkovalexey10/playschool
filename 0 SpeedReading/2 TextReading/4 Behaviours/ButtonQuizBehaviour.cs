﻿using Components;
using DesertImage;
using DesertImage.Extensions;
using Game.Vibration;
using Interaction;
using Kiddopia.SpeedReading.DataComponents;
using Kiddopia.SpeedReading.Events;
using Kiddopia.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Behaviour = DesertImage.Behaviours.Behaviour;

namespace Kiddopia.SpeedReading.Behaviours
{
    public class ButtonQuizBehaviour : Behaviour, IListen<ClickedEvent>, IListen<ButtonQuizSuccessEvent>,
        IListen<ButtonQuizErrorEvent>, IListen<ButtonQuizDefaultEvent>
    {
        private TextMeshProUGUI _TMPro;
        private Image _borderImageButton;
        private DataQuizButton _button;
        private DataTextReadingStarter _starter;

        public override void Activate()
        {
            _starter = Core.Instance.Get<DataTextReadingStarter>();
            _TMPro = Entity.Get<DataTMP>().Value;
            _borderImageButton = Entity.Get<DataImage>().Value;
            _button = Entity.Get<DataQuizButton>();
            base.Activate();
            Entity.ListenEvent<ClickedEvent>(this);
            Entity.ListenEvent<ButtonQuizSuccessEvent>(this);
            Entity.ListenEvent<ButtonQuizErrorEvent>(this);
            this.ListenGlobalEvent<ButtonQuizDefaultEvent>();
            
        }

        public override void Deactivate()
        {
            base.Deactivate();
            Entity.UnlistenEvent<ClickedEvent>(this);
            Entity.UnlistenEvent<ButtonQuizSuccessEvent>(this);
            Entity.UnlistenEvent<ButtonQuizErrorEvent>(this);
            this.UnlistenGlobalEvent<ButtonQuizDefaultEvent>();
        }

        public void handleCallback(ClickedEvent arguments)
        {
            if(!_starter.IsTapAnswer) return;
            this.SendGlobalEvent(new VibrationSingleEvent());
            this.SendGlobalEvent(new TapButtonPanelEvent()
            {
                CurrentTextButton = _TMPro.text,
                CurrentButton = Entity
            });
            _starter.IsTapAnswer = false;
        }

        public void handleCallback(ButtonQuizSuccessEvent arguments)
        {
            _borderImageButton.sprite = _button.Borders[0];
            _TMPro.color = Color.green;
            _starter.IsTapAnswer = false;
        }

        public void handleCallback(ButtonQuizErrorEvent arguments)
        {
            _borderImageButton.sprite = _button.Borders[1];
            _TMPro.color = Color.red;
            _starter.IsTapAnswer = true;
        }

        public void handleCallback(ButtonQuizDefaultEvent arguments)
        {
            _borderImageButton.sprite = _button.Borders[2];
            _TMPro.color = Color.black;
            _starter.IsTapAnswer = true;
        }
    }
}