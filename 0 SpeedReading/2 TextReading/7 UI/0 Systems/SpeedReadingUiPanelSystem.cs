﻿using DesertImage;
using DesertImage.Extensions;
using DesertImage.Managers;
using Kiddopia.Audio;
using Kiddopia.Loading;
using Kiddopia.Painting;
using Kiddopia.SpeedReading.DataComponents;
using Kiddopia.SpeedReading.Events;
using Kiddopia.UI;
using MathWhiz;
using UI;
using UI.Extensions;

namespace Kiddopia.SpeedReading.UI
{
    public class SpeedReadingUiPanelSystem : SystemBase, IListen<PlayTextCompleteEvent>, IListen<PlayEvent>, IListen<RestartPaintEvent>, IListen<WinEvent>
    {
        private DataTextReadingQuiz _quiz;
        private DataTextReadingStarter _starter;
        public override void Activate()
        {
            base.Activate();
            _quiz = Core.Instance.Get<DataTextReadingQuiz>();
            _starter = Core.Instance.Get<DataTextReadingStarter>();

            
            this.ShowScreen(UIIDs.LearningTopPanel, new LearningTopPanelSettings()
            {
                Text = _quiz.Texts[_starter.CurrentText-1].NameText,
                SoundIds = new[] { (ushort)SoundId.TextReadingBegin }
            });
            this.ShowScreen(UIIDs.StartPanel);
            this.ShowScreen(UIIDs.BackPanel, new CustomBackPanelSettings
                {OnClick = () => LoadingController.Load((ushort)ScenesId.SpeedReadingSelector)});
            
            
            this.ListenGlobalEvent<PlayTextCompleteEvent>();
            this.ListenGlobalEvent<PlayEvent>();
            this.ListenGlobalEvent<RestartPaintEvent>();
            this.ListenGlobalEvent<WinEvent>();
        }

        public override void Deactivate()
        {
            base.Deactivate();
            this.UnlistenGlobalEvent<PlayTextCompleteEvent>();
            this.UnlistenGlobalEvent<PlayEvent>();
            this.UnlistenGlobalEvent<RestartPaintEvent>();
            this.UnlistenGlobalEvent<WinEvent>();
        }

        public void handleCallback(PlayTextCompleteEvent arguments)
        {
            this.HideScreen(UIIDs.StartPanel);
            this.HideScreen(UIIDs.RestartPaintPanel);
            this.ShowScreen(UIIDs.LearningTopPanel, new LearningTopPanelSettings()
            {
                Text = _quiz.Texts[_starter.CurrentText-1].NameText,
                SoundIds = new[] { (ushort)SoundId.TextReadingEnd },
                SoundAutoStart = false
            });
        }

        public void handleCallback(PlayEvent arguments)
        {
            this.HideScreen(UIIDs.StartPanel);
            this.ShowScreen(UIIDs.RestartPaintPanel);
        }

        public void handleCallback(RestartPaintEvent arguments)
        {
            this.ShowScreen(UIIDs.StartPanel);
            this.HideScreen(UIIDs.RestartPaintPanel);
        }

        public void handleCallback(WinEvent arguments)
        {
            this.HideScreen(UIIDs.RestartPaintPanel);
            this.HideScreen(UIIDs.BackPanel);
            this.HideScreen(UIIDs.StartPanel);
            this.HideScreen(UIIDs.LearningTopPanel);
        }
    }
}