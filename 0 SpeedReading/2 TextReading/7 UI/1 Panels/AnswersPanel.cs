﻿using DesertImage.UI.Panel;
using TMPro;
using UI;
using UniRx;
using UnityEngine;

namespace Kiddopia.SpeedReading.UI
{
    public class AnswersPanel : Panel<AnswersPanelSettings>
    {
        [SerializeField] private TextMeshProUGUI[] textsButton;
        public override ushort Id => (ushort)UIIDs.BottomButtonPanel;

        public override PanelPriority Priority => PanelPriority.Master;

        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        public override void Setup(AnswersPanelSettings settings)
        {
            base.Setup(settings);
            
            _disposable.Clear();

            settings.TextsButton
                .Subscribe(value =>
                {
                    for (byte i = 0; i < value.Length; i++)
                    {
                        textsButton[i].text = value[i];
                    }
                })
                .AddTo(_disposable);
            
        }
    }

    public class AnswersPanelSettings : IPanelSettings
    {
        public PanelPriority Priority { get; }

        public ReactiveProperty<string[]> TextsButton;
    }

}