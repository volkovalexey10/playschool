﻿using DesertImage.UI.Panel;
using TMPro;
using UI;
using UniRx;
using UnityEngine;

namespace Kiddopia.SpeedReading.UI
{
    public class StarsPanel : Panel<StarsPanelSettings>
    {
        [SerializeField] private TextMeshProUGUI text;
        public override ushort Id => (ushort)UIIDs.StarsPanel;

        public override PanelPriority Priority => PanelPriority.Master;

        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        
        public override void Setup(StarsPanelSettings settings)
        {
            base.Setup(settings);
            _disposable.Clear();
            
            settings.CountQuestion.Subscribe(value =>
            {
                text.text = $"Question {value}/3";
            }).AddTo(_disposable);

        }
    }

    public class StarsPanelSettings : IPanelSettings
    {
        public PanelPriority Priority { get; }
        
        public ReactiveProperty<byte> CountQuestion;
    }
}