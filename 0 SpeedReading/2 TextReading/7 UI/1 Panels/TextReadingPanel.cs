﻿using DesertImage.UI.Panel;
using TMPro;
using UI;
using UniRx;
using UnityEngine;

namespace Kiddopia.SpeedReading.UI
{
    public class TextReadingPanel : Panel<TextReadingPanelSettings>
    {
        [SerializeField] private TextMeshProUGUI text;
        public override ushort Id => (ushort) UIIDs.TextReadingPanel;

        public override PanelPriority Priority => PanelPriority.Master;

        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        public override void Setup(TextReadingPanelSettings settings)
        {
            base.Setup(settings);
            _disposable.Clear();

            settings.Text.Subscribe(value =>
            {
                text.text = value;
            }).AddTo(_disposable);

        }
    }

    public class TextReadingPanelSettings : IPanelSettings
    {
        public PanelPriority Priority { get; }

        public ReactiveProperty<string> Text;
    }
}