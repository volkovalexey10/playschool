﻿using DesertImage.UI.Panel;
using UI;
using UniRx;
using UnityEngine.UI;

namespace Kiddopia.SpeedReading.UI
{
    public class SliderPanelTextReading : Panel<SliderPanelTextReadingSettings>
    {
        public override ushort Id => (ushort) UIIDs.SliderPanel;
        public override PanelPriority Priority => PanelPriority.Master;
        public Slider Slider;
        
        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        public override void Setup(SliderPanelTextReadingSettings settings)
        {
            base.Setup(settings);
            _disposable.Clear();

            settings.MaxValue
                .Subscribe(value =>
                {
                    Slider.maxValue = value;
                })
                .AddTo(_disposable);
            settings.Value
                .Subscribe(value =>
                {
                    Slider.value = value;
                })
                .AddTo(_disposable);
        }
    }
    public class SliderPanelTextReadingSettings : IPanelSettings
    {
        public PanelPriority Priority { get; }

        public ReactiveProperty<int> MaxValue;
        public ReactiveProperty<int> Value;
    }
}