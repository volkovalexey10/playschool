﻿using System.Collections.Generic;
using System.Linq;
using DesertImage;
using DesertImage.Extensions;
using DesertImage.Managers;
using Dots.Events;
using Game.Vibration;
using Kiddopia.Analytics;
using Kiddopia.GameData;
using Kiddopia.Loading;
using Kiddopia.SpeedReading.Data;
using Kiddopia.SpeedReading.DataComponents;
using Kiddopia.SpeedReading.Events;
using Kiddopia.SpeedReading.UI;
using Other;
using UI;
using UI.Extensions;
using UniRx;
using Random = UnityEngine.Random;

namespace Kiddopia.SpeedReading.Systems
{
    public class SpeedReadingQuizSystem : SystemBase, IListen<PlayTextCompleteEvent>, IListen<TapButtonPanelEvent>
    {
        private DataTextReadingStarter _starter;
        private DataTextReadingQuiz _quiz;
        private AnswersPanelSettings _answersPanelSettings;
        private StarsPanelSettings _starsPanelSettings;
        private TextReadingPanelSettings _textReadingPanelSettings;

        private readonly List<string> _listAnswers = new List<string>();
        private List<byte> _numbersToUse;
        private string _question;
        private byte _maxAnswers;
        private sbyte _successAnswers;

        public override void Activate()
        {
            _starter = Core.Instance.Get<DataTextReadingStarter>();
            _quiz = Core.Instance.Get<DataTextReadingQuiz>();
            _answersPanelSettings = new AnswersPanelSettings {TextsButton = new ReactiveProperty<string[]>()};
            _starsPanelSettings = new StarsPanelSettings { CountQuestion = new ReactiveProperty<byte>() };
            _textReadingPanelSettings = new TextReadingPanelSettings {Text = new ReactiveProperty<string>()};
            _maxAnswers = 0;
            _successAnswers = 0;
            _numbersToUse = new List<byte>();
            
            this.ListenGlobalEvent<PlayTextCompleteEvent>();
            this.ListenGlobalEvent<TapButtonPanelEvent>();
            base.Activate();
        }

        public override void Deactivate()
        {
            base.Deactivate();
            this.UnlistenGlobalEvent<PlayTextCompleteEvent>();
            this.UnlistenGlobalEvent<TapButtonPanelEvent>();
        }


        public void handleCallback(PlayTextCompleteEvent arguments)
        {
            SetRandomQuiz();
            
            _answersPanelSettings.TextsButton.Value = _listAnswers.Shuffle().ToArray();
            _textReadingPanelSettings.Text.Value = _question;
            
            this.ShowScreen(UIIDs.TextReadingPanel, _textReadingPanelSettings);
            this.ShowScreen(UIIDs.BottomButtonPanel, _answersPanelSettings);
            this.ShowScreen(UIIDs.StarsPanel, _starsPanelSettings);
        }

        // the first in _listAnswers - is always correct
        public void handleCallback(TapButtonPanelEvent arguments)
        {
            
            if (arguments.CurrentTextButton.Equals(_listAnswers[0]))
            {
                _maxAnswers++;
                _starsPanelSettings.CountQuestion.Value++;
                _successAnswers++;
                this.SuccessSound();
                arguments.CurrentButton.SendEvent(new ButtonQuizSuccessEvent());
                this.SendGlobalEvent(new VibrationSingleEvent());
                
                this.DoActionWithDelay(CreateNewQuestion, 1, true);

                // Amplitude analytics
                this.SendGlobalEvent(new AmplitudeHook { State = HookState.Success });
            }
            else
            {
                _successAnswers--;
                this.ErrorSound();
                arguments.CurrentButton.SendEvent(new ButtonQuizErrorEvent());
                this.SendGlobalEvent(new VibrationDoubleEvent());
                
                this.DoActionWithDelay(() =>
                {
                    this.SendGlobalEvent(new ButtonQuizDefaultEvent());
                }, 0.3f, true);

                // Amplitude analytics
                this.SendGlobalEvent(new AmplitudeHook { State = HookState.Fail });
            }
            
            if (_maxAnswers >= 3)
            {
                this.DoActionWithDelay(() =>
                {
                    this.HideScreen(UIIDs.TextReadingPanel);
                    this.HideScreen(UIIDs.BottomButtonPanel);
                    this.HideScreen(UIIDs.StarsPanel);
                    this.SendGlobalEvent(new WinEvent());
                    CompleteLvl(_successAnswers);

                }, 1, true);
            }
        }
        

        private void CompleteLvl(sbyte countSuccessAnswers)
        {
            LocalStorage.GameData.DataSpeedReading.PrevPanel = SpeedReadingPanel.Reading;
            
            var dataCompleteText = LocalStorage.GameData.DataSpeedReading.TextReadingData;
            
            if(!dataCompleteText.TryGetValue(_starter.CurrentText, out _))
                LocalStorage.GameData.DataSpeedReading.TextReadingData.Add(_starter.CurrentText, true);
            
            if (countSuccessAnswers >= 3)
            {
                const byte minCoin = 3;
                var coinAdd = (byte)(minCoin + (_starter.CurrentText - 1));
                this.SendGlobalEvent(new LevelCompleteEvent
                {
                    Reward = coinAdd, 
                    OnlyReward = true,
                    UseCustomChangeScene = true,
                    CustomChangeSceneId = ScenesId.SpeedReadingSelector
                    
                });
            }
            else
            {
                this.SendGlobalEvent(new LevelCompleteEvent
                {
                    ShowButtons = false,
                    HideReward = true,
                    UseCustomChangeScene = true,
                    CustomChangeSceneId = ScenesId.SpeedReadingSelector
                });
            }
        }

        private void CreateNewQuestion()
        {
            _listAnswers.Clear();
            SetRandomQuiz();
            _answersPanelSettings.TextsButton.Value = _listAnswers.Shuffle().ToArray();
            _textReadingPanelSettings.Text.Value = _question;
            this.SendGlobalEvent(new ButtonQuizDefaultEvent());
        }
        
        private void SetRandomQuiz()
        {
            var quiz = GetCurrentQuiz();
            var randomNumber = (byte)Random.Range(0, quiz.Length - 1);

            while (_numbersToUse.Contains(randomNumber))
            {
                randomNumber = (byte)Random.Range(0, quiz.Length - 1);
            }

            for (byte i = 0; i < quiz[randomNumber].Answers.Length; i++)
            {
                _listAnswers.Add(quiz[randomNumber].Answers[i]);
            }
            _question = quiz[randomNumber].Question;
            _numbersToUse.Add(randomNumber);
        }
        
        private IQuiz[] GetCurrentQuiz()
        {
            return _starter.CurrentText switch
            {
                1 => _quiz.QuizMary,
                2 => _quiz.QuizTheWonderful,
                3 => _quiz.QuestionsTheSecretGarden,
                _ => null
            };

        }
    }
}