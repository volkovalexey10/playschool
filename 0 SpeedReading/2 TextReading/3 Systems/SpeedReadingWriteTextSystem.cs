﻿using System;
using DesertImage;
using DesertImage.Audio;
using DesertImage.Extensions;
using DesertImage.Managers;
using Kiddopia.Audio;
using Kiddopia.GameData;
using Kiddopia.Painting;
using Kiddopia.Spawning;
using Kiddopia.SpeedReading.DataComponents;
using Kiddopia.SpeedReading.Events;
using Kiddopia.SpeedReading.UI;
using UI;
using UI.Extensions;
using UniRx;

namespace Kiddopia.SpeedReading.Systems
{
    public class SpeedReadingWriteTextSystem : SystemBase, IListen<RestartPaintEvent>, IListen<PlayEvent>
    {
        private DataTextReadingStarter _starter;
        private DataTextReadingQuiz _quiz;
        private SliderPanelTextReadingSettings _settingsSlider;
        private TextReadingPanelSettings _textReadingPanelSettings;
        
        private char[] _separators;
        private string[] _strings;
        private const string _beginTag = "<color=#FF742C>";
        private const string _endTag = "</color>";
        private float _speedText;

        public override void Activate()
        {
            base.Activate();
            _starter = Core.Instance.Get<DataTextReadingStarter>();
            _starter.CurrentText = LocalStorage.GameData.DataSpeedReading.CurrentText;
            _quiz = Core.Instance.Get<DataTextReadingQuiz>();
            _strings = GetText().Split(new []{'|'}, StringSplitOptions.RemoveEmptyEntries);
            _separators = new[] {' ', ',', '.', '?', ':', ';', '—', '‘', '’'};
            _textReadingPanelSettings = new TextReadingPanelSettings {Text = new ReactiveProperty<string>()};
            _settingsSlider = new SliderPanelTextReadingSettings
            {
                MaxValue = new ReactiveProperty<int>(),
                Value = new ReactiveProperty<int>()
            };
            
            this.ListenGlobalEvent<PlayEvent>();
            this.ListenGlobalEvent<RestartPaintEvent>();
            
            CreateSpeedText();
            CreateSentence();
        }

        public override void Deactivate()
        {
            base.Deactivate();
            this.UnlistenGlobalEvent<PlayEvent>();
            this.UnlistenGlobalEvent<RestartPaintEvent>();
        }
        
        

        public void handleCallback(RestartPaintEvent arguments)
        {
            this.CancelOwnedDoAction();
            _settingsSlider.Value.Value = 0;
            _textReadingPanelSettings.Text.Value = _strings[0];
            
        }

        public void handleCallback(PlayEvent arguments)
        {
            PlayText();
        }

        private void PlayText()
        {
            float totalDuration = 0;
            float totalStrDuration = 0;
            var sound = new SoundBase();
            
            foreach (var str in _strings)
            {
                _textReadingPanelSettings.Text.Value = str;
                var words = str.Split(new []{" "}, StringSplitOptions.RemoveEmptyEntries);
                var startIndex = 0;
                var lastIndex = 0;
                float prevWordDuration = 0;
                var prevWordLength = 0;
                
                foreach (var word in words)
                {
                    var wordTrim = word.Trim(_separators);
                    var duration = prevWordDuration + prevWordLength * _speedText;
                    prevWordDuration = duration;
                    prevWordLength = wordTrim.Length;
                    totalDuration += wordTrim.Length * _speedText;

                    this.DoActionWithDelay(() =>
                            {
                                startIndex = str.IndexOf(wordTrim, lastIndex);
                                lastIndex = startIndex + wordTrim.Length;
                                _textReadingPanelSettings.Text.Value = str.Insert(lastIndex, _endTag).Insert(startIndex, _beginTag);
                            },totalStrDuration + duration);

                }
                totalStrDuration = totalDuration;

                this.DoActionWithDelay(() => _settingsSlider.Value.Value++, totalStrDuration);


            }

            this.DoActionWithDelay(() =>
            {
                sound = SoundId.TextReadingEnd.Play2D();
            }, totalDuration);
            
            this.DoActionWithDelay(() =>
            {
                this.HideScreen(UIIDs.TextReadingPanel);
                this.HideScreen(UIIDs.SliderPanel);
                this.SendGlobalEvent(new PlayTextCompleteEvent());
            }, totalDuration + sound.Delay);
        }

        private void CreateSentence()
        {
            _textReadingPanelSettings.Text.Value = _strings[0];
            this.ShowScreen(UIIDs.TextReadingPanel,_textReadingPanelSettings);
            _settingsSlider.MaxValue.Value = _strings.Length;
            this.ShowScreen(UIIDs.SliderPanel, _settingsSlider);

        }

        private void CreateSpeedText()
        {
            switch (_starter.CurrentText)
            {
                case 1:
                    _speedText = 0.1f;
                    return;
                case 2:
                    _speedText = 0.05f;
                    return;
                case 3:
                    _speedText = 0.04f;
                    return;
            }
        }
        // Text is selected from left to right
        private string GetText()
        {
            return _quiz.Texts[_starter.CurrentText - 1].Text;
        }
    }
}