using System.Collections;
using DesertImage.Audio;
using DesertImage.Starters;
using Game;
using GameInput;
using Kiddopia.UI;
using Kiddopia.GameData;
using UnityEngine;
using Dots.Systems;
using Game.Vibration;
using Kiddopia.AgeConfirm.UI;
using Kiddopia.Analytics;
using Kiddopia.Audio;
using Kiddopia.Economic;
using Kiddopia.IAP;
using Kiddopia.Reward;
using Kiddopia.SpeedReading.DataComponents;
using Kiddopia.SpeedReading.Systems;
using Kiddopia.SpeedReading.UI;
using Statistic;
using Subscription;
using Vibration.DataComponents;

namespace Kiddopia.Starters
{
    public class TextReadingStarter : Starter
    {
        protected override IEnumerator InitData()
        {
            var process = base.InitData();

            while (process.MoveNext())
            {
                yield return null;
            }

            Core.Add<DataGameMode>().Value = GameModes.SpeedReading;

            Core.Add<DataUIManager>();

            Core.Add<DataSoftMoney>();
            Core.Add<DataHardMoney>();
            
            Core.Add<DataVibration>();
        }

        protected override IEnumerator InitSystems()
        {
            var process = base.InitSystems();

            while (process.MoveNext())
            {
                yield return null;
            }

            var speedReadingData = Core.Get<DataTextReadingStarter>();
            while (speedReadingData == null)
            {
                speedReadingData = Core.Get<DataTextReadingStarter>();
            }

            yield return Resources.UnloadUnusedAssets();
            
            Core.Add<SaveLoadSystem>();
            Core.Add<AutoSaveSystem>();

            StatisticPack.Integrate(Core);
            RewardStuffPack.AddTo(Core);
            
            Core.Add<IAPInitializeSystem>(); // why? 
            Core.Add<IAPBuySystem>(); // why? 
            
            Core.Add<SubscriptionInitSystem>();
            Core.Add<SubscriptionManagingSystem>();
            
            Core.Add<EconomicMoneyManagingSystem>();
            Core.Add<EconomicMoneySaveLoadSystem>();
            
            Core.Add<UIControllerSystem>();
            
            Core.Add<UIAgeConfirmSystem>();
            
            Core.Add<SoundPlayWhenClickEmptySpaceSystem2D>();
            
            Core.Add<InputStandaloneSystem>();
            
            Core.Add<VibrationHapticSystem>();
            
            Core.Add<MusicSystem>();
            Core.Add<LevelCompleteSystem>();
            
            
            Core.Add<SpeedReadingWriteTextSystem>();
            Core.Add<SpeedReadingUiPanelSystem>();
            Core.Add<SpeedReadingQuizSystem>();

            // Amplitude analytics
            Core.Add<AmplitudeSystem>();
            Core.Add<AmplitudeIAPEventsSupervisor>();
            Core.Add<AmplitudeLevelCompleteSupervisor>().IgnoreMenuHistory();
            Core.Add<AmplitudeSpeedReadingTextSupervisor>();

        }

        protected override IEnumerator InitManagers()
        {
            var process = base.InitManagers();

            while (process.MoveNext())
            {
                yield return null;
            }

            Core.Add<MusicPlayer>();
        }
    }
}
