﻿using DesertImage.Entities;

namespace Kiddopia.SpeedReading.Events
{
    public struct TapButtonPanelEvent
    {
        public string CurrentTextButton;
        public IEntity CurrentButton;
    }
}