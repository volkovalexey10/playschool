﻿using UnityEngine;

namespace Kiddopia.SpeedReading
{
    [CreateAssetMenu(fileName = "QuizReading", menuName = "Kiddopia/SpeedReading/QuizReading")]
    public class ScriptableSpeedReadingQuiz : ScriptableSpeedReadingItem, IQuiz
    {
        public string Question => questions;
        public string[] Answers => answers;

        [SerializeField] private string questions;
        [SerializeField] private string[] answers;
        
    }
}