﻿namespace Kiddopia.SpeedReading
{
    public interface ISpeedReadingText : ISpeedReadingItem
    {
        string Text { get; }
        string NameText { get; }

    }
}