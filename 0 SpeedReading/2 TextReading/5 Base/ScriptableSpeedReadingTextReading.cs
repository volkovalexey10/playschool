﻿using UnityEngine;

namespace Kiddopia.SpeedReading
{
    [CreateAssetMenu(fileName = "TextReading", menuName = "Kiddopia/SpeedReading/TextReading")]
    public class ScriptableSpeedReadingTextReading : ScriptableSpeedReadingItem, ISpeedReadingText
    {
        public string NameText => nameText;
        public string Text => text;
        
        
        [SerializeField] private string text;
        [SerializeField] private string nameText;
    }
}