﻿namespace Kiddopia.SpeedReading
{
    public interface IQuiz : ISpeedReadingItem
    {
        public string Question { get; }
        public string[] Answers { get; }
    }
}