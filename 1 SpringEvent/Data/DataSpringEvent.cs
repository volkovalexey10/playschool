﻿using System;

namespace Kiddopia.SpringEvent
{
    [Serializable]
    public struct DataSpringEvent
    {
        public bool IsStartEvent;
        public bool IsCompletetEvent;
        public string TimeComplete;
    }
}