﻿using System;
using Components;
using Game.Base;
using Kiddopia.Game;
using Kiddopia.Loading;

namespace Kiddopia.SpringEvent
{
    [Serializable]
    public class DataSpringGameButton : DataComponent<DataSpringGameButton>
    {
        public ScriptableMenuItem icon;
        public ScriptableMenuItemsGroup tasksGroup;
        public ScenesId scenesId;
        public int choosedSelectorLevel;
        public Game gameMode;
    }

    public enum Game
    {
        Gardener,
        Vase,
        Rhymes,
        Time
    }
}