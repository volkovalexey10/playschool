﻿using System;
using Components;
using Game.Base;

namespace Kiddopia.SpringEvent
{
    [Serializable]
    public class DataSpringEventIcons : DataComponent<DataSpringEventIcons>
    {
        public IMenuItem[] IconsToOpen;
        public IMenuItem[] IconsToActiveNew;


    }
}