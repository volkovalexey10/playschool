﻿using System;
using System.Globalization;
using DesertImage;
using DesertImage.Extensions;
using DesertImage.Managers;
using Kiddopia.GameData;
using Kiddopia.Menu;
using UI;
using UI.Extensions;
using UniRx;

namespace Kiddopia.SpringEvent
{
    public class SpringShowPanelSystem : SystemBase, IListen<MenuStateSetEvent>, ITickFixed
    {
        private SpringEventPanelSettings _panelSettings;
        private SpringEventButtonSettings _buttonSettings;
        private DateTime _completeTime;
        private TimeSpan _currentTimeSpan;
        private bool _isOpenPanel;
        private bool _isActiveEvent;
        private int _weeks;
        private int _days;

        public override void Activate()
        {
            base.Activate();
            
            DateTime.TryParse(LocalStorage.GameData.DataSpringEvent.TimeComplete, CultureInfo.InvariantCulture, DateTimeStyles.None, out _completeTime);
            _isActiveEvent = LocalStorage.GameData.DataSpringEvent.IsStartEvent;
            
            this.ListenGlobalEvent<MenuStateSetEvent>();
            
            CreatePanelTime();
            ShowEvent();
        }

        public override void Deactivate()
        {
            base.Deactivate();
            this.UnlistenGlobalEvent<MenuStateSetEvent>();
        }

        public void FixedTick()
        {
            if(!_isOpenPanel) return;
            CreatePanelTime();
            _panelSettings.TextPanel.Value =
                $"{_weeks}w {_days}d {_currentTimeSpan.Hours}h {_currentTimeSpan.Minutes}m";
        }

        private void ShowEvent()
        {
            if (!_isActiveEvent) return;
            _isOpenPanel = true;
            _buttonSettings = new SpringEventButtonSettings
            {
                OnClick = () =>
                {
                    this.HideScreen(UIIDs.SpringButtonPanel);
                    this.ShowScreen(UIIDs.SpringPanelEvent, _panelSettings);
                    _isOpenPanel = true;
                },
                TextTime = new ReactiveProperty<string>($"{_weeks}w {_days}d")

            };
            _panelSettings = new SpringEventPanelSettings
            {
                OnClickClose = () =>
                {
                    this.HideScreen(UIIDs.SpringPanelEvent);
                    this.ShowScreen(UIIDs.SpringButtonPanel, _buttonSettings);
                    _isOpenPanel = false;
                },
                TextPanel = new ReactiveProperty<string>(
                    $"{_weeks}w {_days}d {_currentTimeSpan.Hours}h {_currentTimeSpan.Minutes}m")
            };

            if (LocalStorage.IsShowSpringPanel)
            {
                this.ShowScreen(UIIDs.SpringButtonPanel, _buttonSettings);
            }
            else
            {
                this.ShowScreen(UIIDs.SpringPanelEvent, _panelSettings);
                LocalStorage.IsShowSpringPanel = true;
            }

        }

        public void handleCallback(MenuStateSetEvent arguments)
        {
            if (!LocalStorage.GameData.DataSpringEvent.IsStartEvent) return;
            switch (arguments.Value)
            {
                case MenuState.Menu:
                    this.ShowScreen(UIIDs.SpringButtonPanel, _buttonSettings);
                    break;

                default:
                    this.HideScreen(UIIDs.SpringButtonPanel);
                    break;
            }
        }

        private void CreatePanelTime()
        {
            _currentTimeSpan = _completeTime.Subtract(DateTime.Now);
            _weeks = _currentTimeSpan.Days / 7;
            _days = _currentTimeSpan.Days - _weeks * 7;

        }
    }
}