﻿using System;
using System.Globalization;
using DesertImage.Managers;
using Kiddopia.GameData;

namespace Kiddopia.SpringEvent
{
    public class SpringEventStartSystem : SystemBase
    {
        private bool _isComplete;
        private bool _isStartEvent;
        private TimeSpan _deadlineEvent;
        
        public override void Activate()
        {
            base.Activate();
            _isComplete = LocalStorage.GameData.DataSpringEvent.IsCompletetEvent;
            _isStartEvent = LocalStorage.GameData.DataSpringEvent.IsStartEvent;
            _deadlineEvent = new TimeSpan(14, 0, 0, 0,0);
            //StartEvent();
            StopEvent();
        }

        private void StartEvent()
        {
            if(_isComplete) return;
            if(_isStartEvent) return;
            
            var startEvent = DateTime.Now;
            var completeTimeEvent = startEvent.Add(_deadlineEvent);
            LocalStorage.GameData.DataSpringEvent.IsStartEvent = true;
            LocalStorage.GameData.DataSpringEvent.TimeComplete = completeTimeEvent.ToString(CultureInfo.InvariantCulture);
        }

        private void StopEvent()
        {
            if(!_isStartEvent) return;

            DateTime.TryParse(LocalStorage.GameData.DataSpringEvent.TimeComplete, CultureInfo.InvariantCulture, DateTimeStyles.None, out var completeTimeEvent);
            var result = DateTime.Compare(DateTime.Now, completeTimeEvent);
            if (result == 0 || result > 0)
            {
                LocalStorage.GameData.DataSpringEvent.IsCompletetEvent = true;
                LocalStorage.GameData.DataSpringEvent.IsStartEvent = false;
            }
        }
    }
}