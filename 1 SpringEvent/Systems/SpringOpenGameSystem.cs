﻿using System.Collections.Generic;
using DesertImage;
using DesertImage.Extensions;
using DesertImage.Managers;
using Game;
using Game.Data;
using Interaction;
using Kiddopia.Game;
using Kiddopia.GameData;
using Kiddopia.Loading;
using Kiddopia.UI;
using UI;
using UI.Extensions;

namespace Kiddopia.SpringEvent
{
    public class SpringOpenGameSystem : SystemBase, IListen<ClickedEvent>, IListen<CreateIconEvent>
    {
        private DataSpringEventIcons _iconsData;
        private DataGameModes _dataGameModes;
        private bool _isActiveEvent;

        public override void Activate()
        {
            base.Activate();
            _iconsData = Core.Instance.Get<DataSpringEventIcons>();
            _dataGameModes = Core.Instance.Get<DataGameModes>();
            _isActiveEvent = LocalStorage.GameData.DataSpringEvent.IsStartEvent;
            this.ListenGlobalEvent<ClickedEvent>();
            this.ListenGlobalEvent<CreateIconEvent>();
            CheckActiveEvent();
        }

        public override void Deactivate()
        {
            base.Deactivate();
            this.UnlistenGlobalEvent<ClickedEvent>();
            this.UnlistenGlobalEvent<CreateIconEvent>();
        }

        public void handleCallback(CreateIconEvent arguments)
        {
            if(!_isActiveEvent) return;
            foreach (var icon in _iconsData.IconsToActiveNew)
            {
                if (icon.Name == arguments.Icon.Name)
                {
                    arguments.ModeWidget.ImageSpringEvent.enabled = true;
                    return;
                }
                else
                {
                    arguments.ModeWidget.ImageSpringEvent.enabled = false;
                }
            }
        }
        public void handleCallback(ClickedEvent arguments)
        {
            if(!_isActiveEvent) return;
            var data = arguments.Value.Get<DataSpringGameButton>();
            var indexHistory = 0;
            var indexCurrentData = 0;
            var isNeedSubMenu = false;
            var isLoadScene = true;
            this.HideScreen(UIIDs.SpringButtonPanel);
            this.HideScreen(UIIDs.SpringPanelEvent);
            this.HideScreen(UIIDs.BottomButtonPanel);
            LocalStorage.MenuNavigationData.History ??= new Stack<ModesHistoryData>();
            
            switch (data.gameMode)
            {
                case Game.Gardener:
                    indexHistory = 4;
                    indexCurrentData = 7;
                    isNeedSubMenu = true;
                    break;
                
                case Game.Vase:
                    indexHistory = 5;
                    LocalStorage.MenuNavigationData.CurrentData = new ModesHistoryData
                    {
                        Index = indexHistory,
                        Items = _dataGameModes.Values
                    };
                    break;
                
                case Game.Rhymes:
                    indexHistory = 2;
                    indexCurrentData = 11;
                    isNeedSubMenu = true;
                    isLoadScene = false;
                    break;
                
                case Game.Time:
                    indexHistory = 3;
                    indexCurrentData = 15;
                    isLoadScene = false;
                    isNeedSubMenu = true;
                    break;
            }
            var modesGroup = _dataGameModes.Values[indexHistory] as IMenuItemsGroup;
            var modes = new ModesHistoryData
            {
                Index = indexHistory,
                Items = _dataGameModes.Values
            };
            
            if (isNeedSubMenu)
            {
                LocalStorage.MenuNavigationData.CurrentData = new ModesHistoryData
                {
                    Index = indexCurrentData,
                    Items = modesGroup.GameModes
                };
            }

            if (isLoadScene)
            {
                LocalStorage.MenuNavigationData.History.Push(modes);
                LocalStorage.MenuNavigationData.ChoosedSelectorLevel = data.choosedSelectorLevel;
                LoadingController.Load((ushort) data.scenesId);
            }
            else
            {
                var modesTask_1 = new ModesHistoryData
                {
                    Index = indexHistory,
                    Items = _dataGameModes.Values
                };
                var modesTask_2 = new ModesHistoryData
                {
                    Index = indexCurrentData,
                    Items = modesGroup.GameModes
                };
                _dataGameModes.History.Push(modesTask_1);
                _dataGameModes.History.Push(modesTask_2);
                
                this.SendGlobalEvent(new MechanicTasksShowEvent
                {
                    Values = data.tasksGroup.GameModes,
                    IsDoNotStack = true
                });
            }
        }

        private void CheckActiveEvent()
        {
            if (_isActiveEvent)
            {
                OpenGameMode();
            }
            else
            {
                CloseGameMode();
            }
        }
        private void OpenGameMode()
        {
            foreach (var icon in _iconsData.IconsToOpen)
            {
                icon.Unlock();
            }
        }

        private void CloseGameMode()
        {
            foreach (var icon in _iconsData.IconsToOpen)
            {
                icon.Lock();
            }
        }

    }
}