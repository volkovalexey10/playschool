﻿using System;
using DesertImage.UI.Panel;
using Extensions;
using TMPro;
using UI;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Kiddopia.SpringEvent
{
    public class SpringEventButtonPanel : Panel<SpringEventButtonSettings>
    {
        [SerializeField] private Button button;
        [SerializeField] private TextMeshProUGUI textMeshPro;
        
        public override ushort Id => (ushort) UIIDs.SpringButtonPanel;

        public override PanelPriority Priority => PanelPriority.Master;
        
        private readonly CompositeDisposable _disposable = new CompositeDisposable();


        public override void Setup(SpringEventButtonSettings settings)
        {
            base.Setup(settings);
            _disposable.Clear();
            
            button.SetOnClick(settings.OnClick);
            settings.TextTime.Subscribe(text =>
            {
                textMeshPro.text = text;
            }).AddTo(_disposable);
        }
    }

    public class SpringEventButtonSettings : IPanelSettings
    {
        public PanelPriority Priority { get; }
        public Action OnClick;
        public ReactiveProperty<string> TextTime;

    }
}