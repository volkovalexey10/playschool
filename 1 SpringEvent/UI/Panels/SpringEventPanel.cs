﻿using System;
using DesertImage.UI.Panel;
using Extensions;
using TMPro;
using UI;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Kiddopia.SpringEvent
{
    public class SpringEventPanel : Panel<SpringEventPanelSettings>
    {
        [SerializeField] private Button closeButton;
        [SerializeField] private TextMeshProUGUI textTimeEvent;
        public override ushort Id => (ushort)UIIDs.SpringPanelEvent;

        public override PanelPriority Priority => PanelPriority.Master;
        
        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        public override void Setup(SpringEventPanelSettings settings)
        {
            base.Setup(settings);
            _disposable.Clear();
            closeButton.SetOnClick(settings.OnClickClose);
            settings.TextPanel.Subscribe(value =>
            {
                textTimeEvent.text = value;
            }).AddTo(_disposable);
        }
    }

    public class SpringEventPanelSettings : IPanelSettings
    {
        public PanelPriority Priority { get; }
        public Action OnClickClose;
        public ReactiveProperty<string> TextPanel;
    }
}