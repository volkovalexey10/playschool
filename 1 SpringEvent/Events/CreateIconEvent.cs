﻿using Game.Base;
using Menu.UI;

namespace Kiddopia.SpringEvent
{
    public struct CreateIconEvent
    {
        public IMenuItem Icon;
        public ModeWidget ModeWidget;
    }
}