﻿using Framework.Components;
using Game.Base;
using UnityEngine;

namespace Kiddopia.SpringEvent
{
    public class DataSpringEventWrapper : ComponentWrapper<DataSpringEventIcons>
    {
        [SerializeField] private ScriptableMenuItem[] iconsToOpen;
        [SerializeField] private ScriptableMenuItem[] iconsToActiveNew;
        
        protected override DataSpringEventIcons GetData()
        {
            data.IconsToOpen = iconsToOpen;
            data.IconsToActiveNew = iconsToActiveNew;
            
            return base.GetData();
        }
    }
}