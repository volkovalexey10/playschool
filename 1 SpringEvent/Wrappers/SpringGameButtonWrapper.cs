﻿using Components;
using UnityEngine;

namespace Kiddopia.SpringEvent
{
    public class SpringGameButtonWrapper : EntityComponentWrapper
    {
        [SerializeField] private DataSpringGameButton button;
        public override void Link(IComponentHolder componentHolder)
        {
            base.Link(componentHolder);
            componentHolder.Add(button);
        }

    }
}