﻿using Animation;
using Components;
using Kiddopia.CardsShop.Behaviours;
using UnityEngine;

namespace Kiddopia.CardsShop.Wrappers
{
    public class BtnBuyCardWrapper : EntityComponentWrapper
    {
        [SerializeField] private DataAnimator dataAnimator;
        public override void Link(IComponentHolder componentHolder)
        {
            base.Link(componentHolder);
            componentHolder.Add(dataAnimator);
            componentHolder.Add<BtnBuyCardBehaviour>();
        }
    }
}