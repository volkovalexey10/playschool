﻿using Components;
using Kiddopia.CardsShop.Behaviours;

namespace Kiddopia.CardsShop.Wrappers
{
    public class ZoomCardWrapper : EntityComponentWrapper
    {
        public override void Link(IComponentHolder componentHolder)
        {
            base.Link(componentHolder);
            componentHolder.Add<ZoomCardBehaviour>();
        }
    }
}