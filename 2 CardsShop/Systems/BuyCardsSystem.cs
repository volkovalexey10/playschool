﻿using System.Collections;
using DesertImage;
using DesertImage.Extensions;
using DesertImage.Managers;
using DesertImage.Starters;
using Game.Vibration;
using Kiddopia.CardsShop.Data;
using Kiddopia.CardsShop.Events;
using Kiddopia.Economic;
using Kiddopia.GameData;
using Other;

namespace Kiddopia.CardsShop.Systems
{
    public class BuyCardsSystem : SystemBase, IListen<BuyCardEvent>
    {
        private DataCardsShop _dataCardsShop;
        private DataCards _dataCards;
        private DataSoftMoney _dataMoney;
        public override void Activate()
        {
            _dataCardsShop = Core.Instance.Get<DataCardsShop>();
            _dataCards = Core.Instance.Get<DataCards>();
            _dataMoney = Core.Instance.Get<DataSoftMoney>();
            
            base.Activate();
            this.ListenGlobalEvent<BuyCardEvent>();
        }

        public override void Deactivate()
        {
            base.Deactivate();
            this.UnlistenGlobalEvent<BuyCardEvent>();
        }


        public void handleCallback(BuyCardEvent arguments)
        {
            var cardSettings = arguments.Card.CurrentCardSetting;
            
            if (EnoughCoin(cardSettings.Price))
            {
                this.SuccessSound();
                this.SendGlobalEvent(new VibrationSingleEvent());
                
                LocalStorage.GameData.DataCardsShopLocal.PurchasedCards
                    .Add(cardSettings.Name);
                

                var dataPurchased = LocalStorage.GameData.DataCardsShopLocal.PurchasedCards;
                
                arguments.Card.LockCard(false);
                arguments.ZoomCard?.LockCard(false);
                
                cardSettings.IsLock = false;

                var zoomCard = arguments.ZoomCard;

                if (zoomCard != null)
                {
                    var zoomCardCurrentCardSetting = zoomCard.CurrentCardSetting;
                    zoomCardCurrentCardSetting.IsLock = false;
                }
                
                _dataCardsShop.ProgressSlider.value++;
                _dataCardsShop.TMProProgress.text = $"{dataPurchased.Count}/{_dataCards.Cards.Count}";
                
                Starter.Instance.StartCoroutine(
                    AnimationCoins(_dataMoney.Money.Value, cardSettings.Price));
            }
            else
            {
                this.SendGlobalEvent(new NotEnoughCoinEvent { Card = arguments.Card});
            }
        }

        private IEnumerator AnimationCoins(int coins, int price)
        {
            var newCoins = coins - price;

            while (coins != newCoins)
            {
                coins--;
                yield return null;
                yield return null;
                _dataCardsShop.TMProCoin.text = $"{coins}";
            }
            _dataMoney.Money.Value -= price;
        }
        private bool EnoughCoin(int price)
        {
            var coin = _dataMoney.Money.Value;

            return coin >= price;
        }
    }
}