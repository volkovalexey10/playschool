﻿using System.Collections.Generic;
using DesertImage;
using DesertImage.Extensions;
using DesertImage.Managers;
using Kiddopia.GameData;
using Kiddopia.CardsShop.Data;
using Kiddopia.CardsShop.Events;
using Kiddopia.Economic;
using Kiddopia.PrimingSystem.Interface;
using Kiddopia.Spawning;
using UI;
using UI.Extensions;
using UnityEngine;

namespace Kiddopia.CardsShop.Systems
{
    public class CreateCardsShopSystem : SystemBase
    {
        private DataCardsShop _dataCardsShop;
        private DataCards _dataCards;
        private DataSoftMoney _dataMoney;
        private int CountPages
        {
            get
            {
                var pages = _dataCards.Cards.Count % _dataCards.MaxCardsPage == 0
                    ? _dataCards.Cards.Count / _dataCards.MaxCardsPage
                    : _dataCards.Cards.Count / _dataCards.MaxCardsPage + 1;
                return pages;
            }
        }
        public override void Activate()
        {
            _dataCardsShop = Core.Instance.Get<DataCardsShop>();
            _dataCards = Core.Instance.Get<DataCards>();
            _dataMoney = Core.Instance.Get<DataSoftMoney>();
            
            base.Activate();
            CreateCards();
        }

        private void CreateCards()
        {
            var dataPurchased = LocalStorage.GameData.DataCardsShopLocal.PurchasedCards ?? new List<CardsName>();
            var coin = _dataMoney.Money.Value;
            
            var card = 0;
            
            for (var page = 0; page < CountPages; page++)
            {
                var pageContent = ObjectsId.PageCards.SpawnAs<Transform>(_dataCardsShop.PagesContent);
                var countCreateCard = 0;
                
                for (; card < _dataCards.Cards.Count; card++)
                {
                    var cardWidget = ObjectsId.Card.SpawnAs<ICard>(pageContent);
                    
                    var cardSettings = new CardSettings
                    {
                        Icon = _dataCards.Cards[card].Item2.Icon,
                        Name = _dataCards.Cards[card].Item1,
                        OnClickZoom = () => cardWidget.Entity.SendEvent(new ChooseCardEvent {Card = cardWidget}),
                        OnClickBuy = () => this.SendGlobalEvent(new BuyCardEvent
                        {
                            Card = cardWidget,
                            ZoomCard = null
                        }),
                        Price = _dataCards.Cards[card].Item2.Price,
                        Scale = Vector3.one,
                        IsLock = !dataPurchased.Contains(_dataCards.Cards[card].Item1)
                    };
                    
                    cardWidget.Create(cardSettings);
                    
                    countCreateCard++;
                    
                    if (countCreateCard < _dataCards.MaxCardsPage) continue;
                    card++;
                    break;
                }
                
                _dataCardsShop.Scroll.AddChild(pageContent.gameObject);
            }
            
            _dataCardsShop.ProgressSlider.maxValue = _dataCards.Cards.Count;
            _dataCardsShop.ProgressSlider.value = dataPurchased.Count;
            _dataCardsShop.TMProProgress.text = $"{dataPurchased.Count}/{_dataCards.Cards.Count}";
            _dataCardsShop.TMProPages.text = $"Page {_dataCardsShop.Scroll.CurrentPage + 1}/{CountPages}";
            _dataCardsShop.TMProCoin.text = $"{coin}";

            _dataCardsShop.Scroll.OnSelectionPageChangedEvent.AddListener(index =>
            {
                _dataCardsShop.TMProPages.text = $"Page {_dataCardsShop.Scroll.CurrentPage + 1}/{CountPages}";
            });
        }
    }
}