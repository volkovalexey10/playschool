﻿using DesertImage.Extensions;
using DesertImage.UI.Panel;
using Extensions;
using Kiddopia.Menu;
using UI;
using UnityEngine;
using UnityEngine.UI;

namespace Kiddopia.CardsShop.Panels
{
    public class CardsShopBtnPanel : Panel<CardsShopBtnPanelSettings>
    {
        [SerializeField] private Button button;
        public override ushort Id => (ushort) UIIDs.CardsShopButton;

        public override PanelPriority Priority => PanelPriority.Master;

        public override void Initialize()
        {
            base.Initialize();
            button.SetOnClick(() => this.SendGlobalEvent(new MenuStateSetEvent {Value = MenuState.CardsShop}));
        }
    }

    public class CardsShopBtnPanelSettings : IPanelSettings
    {
        public PanelPriority Priority { get; }
    }
}