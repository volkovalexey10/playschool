﻿using DesertImage;
using DesertImage.UI.Panel;
using Kiddopia.CardsShop.Data;
using UI;

namespace Kiddopia.CardsShop.Panels
{
    public class CardZoomPanel : Panel<CardZoomPanelSettings>
    {
        public override ushort Id => (ushort) UIIDs.CardsZoomPanel;

        public override PanelPriority Priority => PanelPriority.Master;

        public override void Initialize()
        {
            Core.Instance.Get<DataCardsShop>().CenterZoomPanel = transform;
            base.Initialize();
        }
    }
    public class CardZoomPanelSettings : IPanelSettings
    {
        public PanelPriority Priority { get; }
    }
}