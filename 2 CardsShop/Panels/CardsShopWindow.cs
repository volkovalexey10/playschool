﻿using DesertImage;
using DesertImage.Extensions;
using DesertImage.UI;
using Extensions;
using Kiddopia.CardsShop.Data;
using Kiddopia.Menu;
using TMPro;
using UI;
using UI.Extensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace Kiddopia.CardsShop.Panels
{
    public class CardsShopWindow : Window<CardsShopWindowSettings>
    {
        [SerializeField] private Button buttonClose;
        [SerializeField] private Button buttonBack;
        [SerializeField] private Transform contentPages;
        [SerializeField] private Slider progressSlider;
        [SerializeField] private TextMeshProUGUI textPages;
        [SerializeField] private TextMeshProUGUI textProgress;
        [SerializeField] private TextMeshProUGUI textCoin;
        [SerializeField] private HorizontalScrollSnap scroll;
        public override ushort Id => (ushort) UIIDs.CardsShopWindow;

        public override void Initialize()
        {
            base.Initialize();
            
            var dataCardsShop = Core.Instance.Get<DataCardsShop>();
            
            dataCardsShop.PagesContent = contentPages;
            dataCardsShop.ProgressSlider = progressSlider;
            dataCardsShop.TMProPages = textPages;
            dataCardsShop.TMProProgress = textProgress;
            dataCardsShop.TMProCoin = textCoin;
            dataCardsShop.Scroll = scroll;
            
            
            buttonClose.SetOnClick(() =>
            {
                this.ShowScreen(UIIDs.CardsShopButton);
                this.SendGlobalEvent(new MenuStateSetEvent {Value = MenuState.Menu});
            });
            buttonBack.SetOnClick(() =>
            {
                this.ShowScreen(UIIDs.CardsShopButton);
                this.SendGlobalEvent(new MenuStateSetEvent {Value = MenuState.Menu});
            });
        }
    }

    public class CardsShopWindowSettings : IWindowSettings
    {
        public bool DontHideIfNotForeground { get; }

        WindowPriority IWindowSettings.Priority => _priority;

        public bool IsPopup { get; }
        
        
        private WindowPriority _priority;
    }
}