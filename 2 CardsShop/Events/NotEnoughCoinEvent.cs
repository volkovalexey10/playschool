﻿using Kiddopia.PrimingSystem.Interface;

namespace Kiddopia.CardsShop.Events
{
    public struct NotEnoughCoinEvent
    {
        public ICard Card;
    }
}