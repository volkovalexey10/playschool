﻿using Kiddopia.PrimingSystem.Interface;

namespace Kiddopia.CardsShop.Events
{
    public struct BuyCardEvent
    {
        public ICard Card;
        public ICard ZoomCard;
    }
}