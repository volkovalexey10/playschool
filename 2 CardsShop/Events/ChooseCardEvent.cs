﻿using Kiddopia.PrimingSystem.Interface;

namespace Kiddopia.CardsShop.Events
{
    public struct ChooseCardEvent
    {
        public ICard Card;
    }
}