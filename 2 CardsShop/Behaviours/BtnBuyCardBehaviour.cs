﻿using Animation;
using DesertImage;
using DesertImage.Behaviours;
using DesertImage.Extensions;
using Kiddopia.CardsShop.Events;

namespace Kiddopia.CardsShop.Behaviours
{
    public class BtnBuyCardBehaviour : Behaviour, IListen<NotEnoughCoinEvent>
    {
        private DataAnimator _animator;
        public override void Activate()
        {
            _animator = Entity.Get<DataAnimator>();
            
            this.ListenGlobalEvent<NotEnoughCoinEvent>();
            base.Activate();
        }

        public override void Deactivate()
        {
            this.UnlistenGlobalEvent<NotEnoughCoinEvent>();
            base.Deactivate();
        }

        public void handleCallback(NotEnoughCoinEvent arguments)
        {
            //TODO: запустить анимацию того что нет хватает денег
        }
    }
}