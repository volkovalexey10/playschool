﻿using DesertImage;
using DesertImage.Extensions;
using Game.Vibration;
using Kiddopia.CardsShop.Data;
using Kiddopia.CardsShop.Events;
using Kiddopia.PrimingSystem.Interface;
using Kiddopia.Spawning;
using UI;
using UI.Extensions;
using UnityEngine;
using Behaviour = DesertImage.Behaviours.Behaviour;

namespace Kiddopia.CardsShop.Behaviours
{
    public class ZoomCardBehaviour : Behaviour, IListen<ChooseCardEvent>
    {
        private DataCardsShop _dataCardsShop;
        public override void Activate()
        {
            _dataCardsShop = Core.Instance.Get<DataCardsShop>();
            
            Entity.ListenEvent<ChooseCardEvent>(this);
            base.Activate();
        }

        public override void Deactivate()
        {
            Entity.UnlistenEvent<ChooseCardEvent>(this);
            base.Deactivate();
        }

        public void handleCallback(ChooseCardEvent arguments)
        {
            this.SendGlobalEvent(new VibrationSingleEvent());
            var currentSettings = arguments.Card.CurrentCardSetting;
            var cardWidget = ObjectsId.Card.SpawnAs<ICard>(_dataCardsShop.CenterZoomPanel);
                    
            var cardSettings = new CardSettings
            {
                Icon = currentSettings.Icon,
                Price = currentSettings.Price,
                OnClickZoom = () =>
                {
                    cardWidget.Entity.ReturnToPool();
                    this.HideScreen(UIIDs.CardsZoomPanel);
                },
                OnClickBuy = () => this.SendGlobalEvent(new BuyCardEvent
                {
                    Card = arguments.Card,
                    ZoomCard = cardWidget
                }),
                IsLock = currentSettings.IsLock,
                Scale = new Vector3(3f,3f,1)
            };
                    
            cardWidget.Create(cardSettings);
            
            this.ShowScreen(UIIDs.CardsZoomPanel);
        }
    }
}