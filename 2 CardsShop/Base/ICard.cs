﻿using DesertImage.Entities;
using Kiddopia.CardsShop.Data;

namespace Kiddopia.PrimingSystem.Interface
{
    public interface ICard
    {
        public IEntity Entity { get; }
        public CardSettings CurrentCardSetting { get; }
        public void Create(CardSettings settings);
        public void LockCard(bool isUnlock);
    }

    
}