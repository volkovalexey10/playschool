﻿using DesertImage.Entities;
using Extensions;
using Kiddopia.CardsShop.Data;
using Kiddopia.PrimingSystem.Interface;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Kiddopia.CardsShop.Widgets
{
    public class CardWidget : MonoBehaviourPoolable, ICard
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI price;
        [SerializeField] private Button buttonZoom;
        [SerializeField] private Button buttonBuy;
        [SerializeField] private GameObject lockIcon;

        public IEntity Entity => GetComponent<IEntity>();
        public CardSettings CurrentCardSetting { get; private set; }

        public void Create(CardSettings settings)
        {
            CurrentCardSetting = settings;
            
            icon.sprite = settings.Icon;
            price.text = settings.Price.ToString();
            buttonZoom.SetOnClick(settings.OnClickZoom);
            buttonBuy.SetOnClick(settings.OnClickBuy);
            lockIcon.SetActive(settings.IsLock);
            transform.localScale = settings.Scale;
        }

        public void LockCard(bool isLock)
        {
            lockIcon.SetActive(isLock);
            
        }
    }
}