﻿using System;
using Components;
using External;
using UnityEngine;

namespace Kiddopia.CardsShop.Data
{
    [Serializable]
    public class DataCards : DataComponent<DataCards>
    {
        public CustomDictionary<CardsName, Card> Cards;
        public byte MaxCardsPage;
    }

    [Serializable]
    public struct Card
    {
        public Sprite Icon;
        public int Price;
    }
    
    public class CardSettings
    {
        public Sprite Icon;
        public int Price;
        public Action OnClickZoom;
        public Action OnClickBuy;
        public bool IsLock;
        public CardsName Name;
        public Vector3 Scale;
    }
    
    public enum CardsName
    {
        //TODO: написать все карточки
        A,
        B,
        C,
        Animal,
        Car,
        Pig,
        Coin,
    }
}