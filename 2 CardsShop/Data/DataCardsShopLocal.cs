﻿using System;
using System.Collections.Generic;

namespace Kiddopia.CardsShop.Data
{
    [Serializable]
    public struct DataCardsShopLocal
    {
        public List<CardsName> PurchasedCards;
    }
}