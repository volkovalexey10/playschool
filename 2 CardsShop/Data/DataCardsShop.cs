﻿using System;
using Components;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace Kiddopia.CardsShop.Data
{
    [Serializable]
    public class DataCardsShop : DataComponent<DataCardsShop>
    {
        [HideInInspector] public Transform PagesContent;
        [HideInInspector] public Slider ProgressSlider;
        [HideInInspector] public TextMeshProUGUI TMProPages;
        [HideInInspector] public TextMeshProUGUI TMProProgress;
        [HideInInspector] public TextMeshProUGUI TMProCoin;
        [HideInInspector] public HorizontalScrollSnap Scroll;
        [HideInInspector] public Transform CenterZoomPanel;
    }
}