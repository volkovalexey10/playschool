﻿using System;
using Components;
using UnityEngine;

namespace CapableKids.Dishwashing.Tutorial.Data
{
    [Serializable]
    public class DataTutorialDish : DataComponent<DataTutorialDish>
    {
        [HideInInspector] public Transform Dish;
        [HideInInspector] public Transform Soap;
        [HideInInspector] public Transform Sponge;
        [HideInInspector] public Transform Towel;
        public Transform Sink;
        public Transform Drainer;
        public Transform Сrane;
    }
}