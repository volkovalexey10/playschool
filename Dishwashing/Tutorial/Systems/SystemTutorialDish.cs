﻿using System;
using System.Collections.Generic;
using CapableKids.Dishwashing.Tutorial.Data;
using DesertImage;
using DesertImage.Extensions;
using DesertImage.Managers;
using DesertImage.Timers;
using DragAndDrop;
using GameInput;
using Kiddopia.Extensions;
using Kiddopia.GameData;
using Kiddopia.Spawning;
using Tutorial.Base;
using Tutorial.Events;
using Tutorial.Widgets;

namespace CapableKids.Dishwashing.Tutorial.Systems
{
    public class SystemTutorialDish : SystemBase, ITutorial, ITickFixed, IListen<EndStepTutorialEvent>,
        IListen<DragStartEvent>, IListen<InputMouseUpEvent>, IListen<StartTutorialEvent>
    {
        public LTSeq LtSeq { get; set; }
        public string Tap { get; set; }
        public string Idle { get; set; }
        public bool IsStartAnim { get; set; }
        public bool IsDragObj { get; set; }
        public bool IsComplete => LocalStorage.GameData.DataLocalCapableKids.IsTutorDishComplete;

        private HandAboveWidget _hand;
        private DataTutorialDish _dataTutorial;
        private Action _tutorial;
        private readonly Queue<Action> _queueActions = new Queue<Action>();
        private bool _isCompleteStep;
        private bool _isPause;
        private float _timeAnim;
        private bool _isDontCycleStep;

        public override void Activate()
        {
            if (IsComplete) return;

            this.ListenGlobalEvent<EndTutorialEvent>();
            this.ListenGlobalEvent<EndStepTutorialEvent>();
            this.ListenGlobalEvent<StartTutorialEvent>();
            this.ListenGlobalEvent<DragStartEvent>();
            this.ListenGlobalEvent<InputMouseUpEvent>();

            _dataTutorial = Core.Instance.Get<DataTutorialDish>();
            LtSeq = LeanTween.sequence();
            Tap = "tap";
            Idle = "idle";
            _isPause = true;
            base.Activate();

            CreateActions();
        }

        public override void Deactivate()
        {
            if (IsComplete) return;

            this.UnlistenGlobalEvent<EndTutorialEvent>();
            this.UnlistenGlobalEvent<EndStepTutorialEvent>();
            this.UnlistenGlobalEvent<StartTutorialEvent>();
            this.UnlistenGlobalEvent<DragStartEvent>();
            this.UnlistenGlobalEvent<InputMouseUpEvent>();

            base.Deactivate();
        }

        public void handleCallback(StartTutorialEvent arguments)
        {
            if (IsComplete) return;

            _isPause = false;
        }

        public void handleCallback(EndStepTutorialEvent arguments)
        {
            if (IsComplete) return;

            StopTutor();
            _isCompleteStep = true;
            _isPause = true;

            if (arguments.IsNextDontMoveStep)
            {
                _isDontCycleStep = true;
                StartTutor();
            }
            else
            {
                _isDontCycleStep = false;
            }
        }

        public void handleCallback(EndTutorialEvent arguments)
        {
            if (IsComplete) return;

            StopTutor();
            LocalStorage.GameData.DataLocalCapableKids.IsTutorDishComplete = true;
        }

        public void handleCallback(DragStartEvent arguments)
        {
            if (IsComplete) return;

            IsDragObj = true;
        }

        public void handleCallback(InputMouseUpEvent arguments)
        {
            if (IsComplete) return;

            IsDragObj = false;
        }

        public void FixedTick()
        {
            if (IsComplete) return;

            if (_isPause) return;

            if (_isDontCycleStep) return;

            if (IsDragObj)
            {
                StopTutor();

                return;
            }

            if (IsStartAnim) return;

            IsStartAnim = true;

            this.DoActionWithDelay(StartTutor, .3f);
        }

        public void StartTutor()
        {
            if (_isCompleteStep)
            {
                _tutorial = _queueActions.Dequeue();
                _isCompleteStep = false;
                _tutorial.Invoke();
            }
            else
            {
                _tutorial.Invoke();
            }
        }

        public void StopTutor()
        {
            if (_hand == null) return;

            _hand.gameObject.LeanCancel();
            _hand.CancelOwnedTimerSequences();

            IsStartAnim = false;
            this.ReturnToPool(ref _hand);
        }

        private void CreateActions()
        {
            _tutorial = FirstStep;

            _queueActions.Enqueue(SecondStep);
            _queueActions.Enqueue(ThirdStep);
            _queueActions.Enqueue(FourthStep);
            _queueActions.Enqueue(FifthStep);
            _queueActions.Enqueue(SixthStep);
        }

        private void CancelSequence()
        {
            if (LtSeq == null) return;

            LeanTween.cancel(LtSeq.id);

            LtSeq = null;
        }

        private void FirstStep()
        {
            _hand = ObjectsId.HandAbove.SpawnAs<HandAboveWidget>(_dataTutorial.Dish.position);

            _timeAnim = _hand.HandSkeleton.skeleton.Data.FindAnimation(Tap).Duration;

            _hand.gameObject.LeanCancel();
            _hand.CancelOwnedTimerSequences();

            _hand
                .AddActionWithDelay(() => _hand.HandSkeleton.SetAnimation(Tap), .3f)
                .AddActionWithDelay(_timeAnim)
                .AddActionWithDelay(() => _hand.Transform.LeanMove(_dataTutorial.Sink.position, .6f))
                .AddActionWithDelay(() => _hand.HandSkeleton.SetAnimation(Tap), .6f)
                .AddActionWithDelay(_timeAnim)
                .AddActionWithDelay(StopTutor);
        }

        private void SecondStep()
        {
            _hand = ObjectsId.HandAbove.SpawnAs<HandAboveWidget>(_dataTutorial.Soap.position);

            _hand.gameObject.LeanCancel();
            _hand.CancelOwnedTimerSequences();

            _hand
                .AddActionWithDelay(() => _hand.HandSkeleton.SetAnimation(Tap), .3f)
                .AddActionWithDelay(_timeAnim)
                .AddActionWithDelay(() => _hand.Transform.LeanMove(_dataTutorial.Dish.position, .6f))
                .AddActionWithDelay(() => _hand.HandSkeleton.SetAnimation(Tap), .6f)
                .AddActionWithDelay(_timeAnim)
                .AddActionWithDelay(StopTutor);
        }

        private void ThirdStep()
        {
            _hand = ObjectsId.HandAbove.SpawnAs<HandAboveWidget>(_dataTutorial.Sponge.position);

            _hand.gameObject.LeanCancel();
            _hand.CancelOwnedTimerSequences();

            _hand
                .AddActionWithDelay(() => _hand.HandSkeleton.SetAnimation(Tap), .3f)
                .AddActionWithDelay(_timeAnim)
                .AddActionWithDelay(() => _hand.Transform.LeanMove(_dataTutorial.Dish.position, .6f))
                .AddActionWithDelay(() => _hand.HandSkeleton.SetAnimation(Tap), .6f)
                .AddActionWithDelay(_timeAnim)
                .AddActionWithDelay(StopTutor);
        }

        private void FourthStep()
        {
            _hand = ObjectsId.HandAbove.SpawnAs<HandAboveWidget>(_dataTutorial.Сrane.position);

            _hand.HandSkeleton.SetAnimation(Tap, true);
        }

        private void FifthStep()
        {
            _hand = ObjectsId.HandAbove.SpawnAs<HandAboveWidget>(_dataTutorial.Towel.position);

            _hand.gameObject.LeanCancel();
            _hand.CancelOwnedTimerSequences();

            _hand
                .AddActionWithDelay(() => _hand.HandSkeleton.SetAnimation(Tap), .3f)
                .AddActionWithDelay(_timeAnim)
                .AddActionWithDelay(() => _hand.Transform.LeanMove(_dataTutorial.Dish.position, .6f))
                .AddActionWithDelay(() => _hand.HandSkeleton.SetAnimation(Tap), .6f)
                .AddActionWithDelay(_timeAnim)
                .AddActionWithDelay(StopTutor);
        }

        private void SixthStep()
        {
            _hand = ObjectsId.HandAbove.SpawnAs<HandAboveWidget>(_dataTutorial.Dish.position);

            _hand.gameObject.LeanCancel();
            _hand.CancelOwnedTimerSequences();

            _hand
                .AddActionWithDelay(() => _hand.HandSkeleton.SetAnimation(Tap), .3f)
                .AddActionWithDelay(_timeAnim)
                .AddActionWithDelay(() => _hand.Transform.LeanMove(_dataTutorial.Drainer.position, .6f))
                .AddActionWithDelay(() => _hand.HandSkeleton.SetAnimation(Tap), .6f)
                .AddActionWithDelay(_timeAnim)
                .AddActionWithDelay(StopTutor);
        }
    }
}